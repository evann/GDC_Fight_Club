﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlareSpikeSpawner : MonoBehaviour {

    float distance = 0.04f;
    public GameObject prefab;
    public Vector2[] lines;
    public GameObject[] linePointsPairs;
    float randomValue = 0.2637685f;
    float radious = 9.97f;
    float degreePerRad = 180/Mathf.PI;


    // Use this for initialization
    void Start () {
        spawnSpikes();

    }
	
	// Update is called once per frame
	void Update () {
		
	}


    void spawnSpikes()
    {
        spawnCircle();
        /*
        foreach (Vector2[] temp in lines)
        {
            spawnLine(temp[0],temp[1]);
        }
        */

        /*
        spawnLine(new Vector2(-7.094f, -6.81f), new Vector2(8.55f, 4.94f));
        spawnLine(new Vector2(-4.796f, -8.462f), new Vector2(9.56f, 2.4f));
        spawnLine(new Vector2(-0.27f, -9.73f), new Vector2(-0.352f, -6.302f));
        */
        for(int i = 1; i < linePointsPairs.Length;)
        {
            spawnLineV3(linePointsPairs[i - 1].transform.position, linePointsPairs[i].transform.position);
            i += 2;
        }


        //spawnLine(new Vector2(, ), new Vector2(, ));

    }


    /*
    pointGameObjectToCenter(gameObject go){
            Vector3 location = this.transform.localPosition;
            //Vector2 // normalized(x,y)
            //go.rotation //new euler from Vector2
        }
    */

    void spawnCircle()
    {
        float degree = 0.0f;
        GameObject temp;
        float surfaceArea = Mathf.PI * radious * 2;
        float distanceInDegreeForm = (distance / surfaceArea) * 360;

        while (degree < 360.0f)
        {
            if (degreeIsValid(degree)) temp = instantiateEdgingByDegrees(prefab, degree*Mathf.PI/180);
            degree += distanceInDegreeForm;
        }
    }

    bool degreeIsValid(float degree)
    {
        if (degree > 8.2 && degree < 14) ;
        else if (degree > 30.5 && degree < 35) ;
        else if (degree > 92 && degree < 119) ;
        else if (degree > 218.5 && degree < 223) ;
        else if (degree > 360) ;
        else {
            // Debug.Log("degree :" +degree);
            return true;
        }
        return false;
    }


    void spawnLineV3(Vector3 startV3, Vector3 endV3)
    {
        spawnLine(new Vector2(startV3.x, startV3.y), new Vector2(endV3.x, endV3.y));
    }

    void spawnLine(Vector2 start, Vector2 end)
    {
        Vector2 line =  end - start;
        float totalDistance = line.magnitude;
        Vector2 tDNormal = line.normalized;

        float distanceTraveled = 0;
        while(distanceTraveled < totalDistance){
            instantiatePosition(prefab, start.x + (tDNormal.x * distanceTraveled), start.y + (tDNormal.y * distanceTraveled));
            distanceTraveled += distance;
        }
        
    }

    GameObject instantiatePosition(GameObject prefab, float x, float y)
    {
        GameObject result;
        Vector2 randomVec;
        randomVec.x = Mathf.Abs((1.54f * randomValue) + (2.46f * (x % 2)) + (14.3f * (y % 3.4f)))%.05f;
        randomVec.y = Mathf.Abs((0.46f * randomValue) + (1.15f * (y % 2)) + (8.3f * (x % 3.4f)))%.05f;

        // At the part in your code that you would like to create a new object
        Vector3 pos = new Vector3(x + randomVec.x, y + randomVec.y, 0f);      // Position in space  (x,y,z)
        Quaternion rotate = getRotation(x, y);  // Angular rotation (x,y,z)
        result = Instantiate(prefab, pos, rotate);      // Create a new instance of “prefab”
                                                        //       at “pos” and at rotation “rotate”
                                                        //result.GetComponent<PointingShimmers>().pointingDestination = this.gameObject;

        result.GetComponent<PointingShimmers>().spawner = this.gameObject;

        return result;
    }


    GameObject instantiateEdgingByDegrees(GameObject prefab, float radians)
    {
        return instantiatePosition(prefab, Mathf.Cos(radians) * radious, Mathf.Sin(radians) * radious);
    }
    


    Quaternion getRotation(float x, float y)
    {
        float tempDX = x - this.transform.position.x;                                 // + Mathf.Sign(-tempDX)*Mathf.PI
        Quaternion rotate = Quaternion.Euler(0, 0, degreePerRad*Mathf.Atan2(-(y-this.transform.position.y), tempDX)*-1 + (Mathf.Sign(tempDX)) * 180);  // Angular rotation (x,y,z)

        return rotate;
    }


}
