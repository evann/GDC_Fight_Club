﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTriggeredCode : MonoBehaviour {

    public GameObject camera;
    public float zPositionTrigget;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (camera.transform.position.z < zPositionTrigget) Destroy(this.gameObject);
	}
}
