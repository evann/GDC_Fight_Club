﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TheAnyKeyCode : MonoBehaviour {


    public string nextStage;
    public float starTime;
    public float endTime;
    SpriteRenderer selfSR;


    // Use this for initialization
    void Start () {
        selfSR = this.GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown("escape")) Application.Quit();
        if (Time.timeSinceLevelLoad > starTime)
        {
            float percentTime = Mathf.Max(0,Mathf.Min(1,((Time.timeSinceLevelLoad - starTime) / (endTime - starTime))));
            selfSR.color = new Color32((byte)206, (byte)206, (byte)206, (byte)(173 * percentTime));
            if (Input.GetKeyDown("space")|| Input.GetKeyDown("a")|| Input.GetKeyDown("s")|| Input.GetKeyDown("g") || Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1) || Input.GetMouseButtonDown(2)) SceneManager.LoadScene(nextStage);
        }

        checkQuickSkip();

        //if (Input.GetKeyDown("space") || Input.GetKeyDown("a") || Input.GetKeyDown("s") || Input.GetKeyDown("g") || Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1) || Input.GetMouseButtonDown(2)) SceneManager.LoadScene("TTEStage");
    }



    void checkQuickSkip()
    {
        if (Input.GetKey("n") && Input.GetKey("1") && Input.GetKeyDown("8")) SceneManager.LoadScene(nextStage);
    }
    

}
