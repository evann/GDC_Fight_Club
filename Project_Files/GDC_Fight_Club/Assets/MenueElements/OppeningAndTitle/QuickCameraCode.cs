﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuickCameraCode : MonoBehaviour {

    public Vector3[] points; // 4 total

    public float[] times; // 5 total

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        float currTime = Time.timeSinceLevelLoad;
        if (currTime> times[0])
        {
            if (currTime > times[1])
            {
                if (currTime > times[2])
                {
                    if (currTime > times[3])
                    {
                        //if (currTime > times[4])
                        //{
                            this.transform.position = points[3];
                        //}
                        //else goBetweenIndexes(3);
                    }
                        else goBetweenIndexes(2);
                }
                        else goBetweenIndexes(1);
            }
                        else goBetweenIndexes(0);
        }
    }

    void goBetweenIndexes(int index)
    {
        float starTime = times[index];
        float endTime = times[index+1];
        float percentTime = ((Time.timeSinceLevelLoad - starTime) / (endTime - starTime));
        if (index == 3) percentTime = Mathf.Pow(percentTime,0.54f); //.075
        Vector3 distance = points[index + 1] - points[index];
        Vector3 nextPos = points[index] + (distance * percentTime);
        this.transform.position = nextPos;
    }
}
