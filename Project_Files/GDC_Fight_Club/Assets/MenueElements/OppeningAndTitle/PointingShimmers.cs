﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointingShimmers : MonoBehaviour {

    bool isInitialized = false;
    public GameObject spawner;
    SpriteRenderer selfSR;
    float randomVal = 0.123f;
    float rate;
    float degreePerRad = 180 / Mathf.PI;



    // Use this for initialization
    void Start () {
        isInitialized = false;
        selfSR = this.GetComponent<SpriteRenderer>();
        selfSR.color = Color.clear;
        setRandom();
    }
	
	// Update is called once per frame
	void Update () {
        /*if (!isInitialized)
        {
            if(pointingDestination != null) pointingDestination = PointingDestination.self;
            if (pointingDestination != null)
            {
                this.transform.parent.transform.LookAt(pointingDestination.transform);
                isInitialized = true;

            }
        }
        else
        {
            selfSR.color = getColor();
            this.transform.parent.transform.LookAt(pointingDestination.transform);

        }
        */
        selfSR.color = getColor();
        if(spawner != null)pointCorrectly();
    }


    Color32 getColor()
    {

        return new Color32((byte)255, (byte)255, (byte)255, (byte)(Mathf.Pow((Mathf.Cos(rate*2f*(Time.time+14)+(randomVal*5))+1),1.5f)*28+5));
    }

    void setRandom()
    {
        Vector3 temp = this.transform.position * 5.6f;
        randomVal = 10*((1.54f *temp.magnitude)+( 2.46f* (temp.x % 2) )+(1.43f* (temp.y % 3.4f)));
        randomVal = ((randomVal%1)+2) %1;
        // Debug.Log(randomVal);

        rate = 0.15f + Mathf.Pow(randomVal,2f) *1.1f;
        if (rate < 0.5f) Destroy(this.gameObject);
        float constantRate = rate * 3.2f;
        this.transform.localScale = new Vector3(constantRate, constantRate, constantRate);
    }

    void pointCorrectly()
    {
        this.transform.rotation = getRotation(spawner.transform.position);
    }

    Quaternion getRotation(Vector2 spawnerV2)
    {
        float tempDX = this.transform.position.x - spawnerV2.x;                                 // + Mathf.Sign(-tempDX)*Mathf.PI
        Quaternion rotate = Quaternion.Euler(0, 0, degreePerRad * Mathf.Atan2(-(this.transform.position.y - spawnerV2.y), tempDX) * -1 + (Mathf.Sign(tempDX)) * 180);  // Angular rotation (x,y,z)

        return rotate;
    }

}
