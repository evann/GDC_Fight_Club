﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlphaTimer : MonoBehaviour {

    public float startTime;
    public float endTime;
    Renderer selfRen;
    public bool faseOut;
    int sign = 0; // 0 if in, 1 if out
    int sign2 = 1;

    public float max;
    public float min;

    Color initialColor;

    // Use this for initialization
    void Start () {
        selfRen = this.GetComponent<Renderer>();
        if (faseOut)
        {
            sign = 1;
            sign2 = -1;
        }
        if (max == 0) max = 1;
        initialColor = selfRen.material.color;

    }
	
	// Update is called once per frame
	void Update () {

        if (Time.timeSinceLevelLoad > endTime) setAlpha(1 - sign);
        else if (Time.timeSinceLevelLoad > startTime)
        {
            setAlpha((((Time.timeSinceLevelLoad - startTime) / (endTime - startTime))-sign)*sign2); // either (x-0)*(1)  OR (x-1)*(-1)
        }
        else setAlpha(sign);
	}


    void setAlpha(float alpha)
    {
        if (alpha <= 0.02) selfRen.material.color = Color.clear;
        else selfRen.material.color = initialColor;
            selfRen.material.SetFloat("_Cutoff", Mathf.Max(min, Mathf.Min(max, 1 - alpha)));
    }
}
