﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeInCode : MonoBehaviour {

    public float starTime;
    public float endTime;
    SpriteRenderer selfSR;
    public Vector3 coloring;


    // Use this for initialization
    void Start()
    {
        selfSR = this.GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {

        if (Time.timeSinceLevelLoad > starTime)
        {
            float percentTime = Mathf.Max(0, Mathf.Min(1, ((Time.timeSinceLevelLoad - starTime) / (endTime - starTime))));
            selfSR.color = new Color32((byte)coloring.x, (byte)coloring.y, (byte)coloring.z, (byte)(255 * percentTime));
        }
    }
}
