﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenueTileMovingBasic : MonoBehaviour {


    public Vector2 direction;
    public float speed;
    Material selfMAt;
    public float scale;


	// Use this for initialization
	void Start () {
        selfMAt = this.GetComponent<Renderer>().material;
        if (scale == 0) scale = 1;
        selfMAt.mainTextureScale = new Vector2(selfMAt.mainTextureScale.x * scale, selfMAt.mainTextureScale.y * scale);

    }
	
	// Update is called once per frame
	void Update () {
        selfMAt.mainTextureOffset = new Vector2(direction.normalized.x * speed * Time.time, direction.normalized.y * speed * Time.time);

    }
}
