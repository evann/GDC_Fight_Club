﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TTECloudCode : MonoBehaviour {

    public float startPoint, endPoint;
    public float speed;
    Vector3 startPos;

	// Use this for initialization
	void Start () {
        startPos = this.transform.position;
        endPoint = endPoint * 1.5f;
        startPoint = startPoint * 1.5f;
        speed = speed * 0.75f;
    }
	
	// Update is called once per frame
	void Update () {
        Vector3 tempV3 = startPos;

        tempV3.x = ((tempV3.x - startPoint + (speed* Time.timeSinceLevelLoad)) )% (endPoint - startPoint);
        tempV3.x = tempV3.x + startPoint;

        this.transform.position = tempV3;

    }


}
