﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackBox : MonoBehaviour {

    public int currAttackNum;
    public CharacterFramework entity;
    public bool modifier01Active;
    public string modifier01;
    public bool modifier02Active;
    public string modifier02;
    public bool modifier03Active;
    public string modifier03;
    public bool modifier04Active;
    public string modifier04;

    public virtual List<string> getModifiers()
    {
        List<string> result = new List<string>();

        if (modifier01Active) result.Add(modifier01);
        if (modifier02Active) result.Add(modifier02);
        if (modifier03Active) result.Add(modifier03);
        if (modifier04Active) result.Add(modifier04);

        return result;
    }

    public virtual string modStartsWith(string stringStart)
    {
        // Used to retrieve a string from the list with a certain starting pair of letters
        if (stringStart == null || stringStart.Length != 2 )
        {
            Debug.LogError("NOT FATAL: NR-> AttackBox.modStartsWith() was given an inadiquit string (must be length 2)");
            return null;
        }
        char[] startCArr = stringStart.ToCharArray();

        foreach (string tempString in getModifiers())
        {
            char[] tempCArr = tempString.ToCharArray();

            if (tempString != null && tempString.Length > 2) 
            {

                if (tempCArr[0] == startCArr[0] && tempCArr[1] == startCArr[1])
                {
                    return tempString;
                }
            }
        }
        return null;
    }



    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
