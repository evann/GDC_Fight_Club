﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine.UI;
using System;


public class CharacterFrameworkWithFileReader : CharacterFramework
{

    protected float[,] attackArray;

    private StringReader srText;
    private StringReader srLine;
    private TextAsset startingMap;
    string currentMapText;


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    // Streaming Assets
    // Flder in unity for files yuo want kept acceable for later
    // path = Application.dataPath + "/StreamingAssets";
    // https://www.raywenderlich.com/479-using-streaming-assets-in-unity


    protected void setAttackArrayFromDocument(string fileName)
    {
        if (attackArray == null) attackArray = new float[15,2];

        //startingMap = getFileFromResources(fileName);
        startingMap = getFileFromStreamingAssets(fileName);
        if (startingMap == null) return;
        currentMapText = startingMap.text;
        if (currentMapText == null) return;
        srText = new StringReader(currentMapText);
        if (srText == null) return;


        getNetxtLine();
        // Get number of inputs
        int numInputs = retrieveInt(3);
        for (int i = 0; i < numInputs; i++)
        {
            completeNextCommand();
        }
    }

    private TextAsset getFileFromResources(string fileName)
    {
        return Resources.Load(fileName) as TextAsset;
    }

    private TextAsset getFileFromStreamingAssets(string fileName)
    {
        string tempString = fileName + ".txt";
        startingMap = null;

        DirectoryInfo directoryInfo = new DirectoryInfo(Application.streamingAssetsPath);
        FileInfo[] allFiles = directoryInfo.GetFiles("*.*");
        foreach (FileInfo tempFile in allFiles)
        {
            if(tempFile.Name == tempString)
            {
                /*
                StartCoroutine("loadTextFromWWW",tempFile);

                
                if (completed != 1) return null;
                */
                IEnumerator coroutine = loadTextFromWWW(tempFile);
                StartCoroutine(coroutine);
                Debug.Log("Found one. (startingMap != null)= " + (startingMap != null )); // ,/  Works!
                break;
            }
        }

        return startingMap;
    }

    IEnumerator loadTextFromWWW(FileInfo tempFileInfo)
    {
        string path = tempFileInfo.FullName.ToString();
        string url = string.Format("file://{0}", path);
        WWW www = new WWW(url);

        startingMap = new TextAsset(www.text);

        yield return null;

    }


    private void completeNextCommand()
    {
        getNetxtLine();
        string key = retrieve2CharKey();
        skipSpaces(1);
        float damage = retrieveInt(5, true) / 100f;
        float knockBackDistance = retrieveInt(5) / 100f;
        setAttackArray(key, damage, knockBackDistance);
    }



    private void setAttackArray(string key, float damage, float knockBackDistance)
    {
        if (key == "SP") vRunSpeed = damage;
        else
        {
            int index = getIndex(key);
            if (index < 0 || index > 14) return;
            attackArray[index, 0] = damage;
            attackArray[index, 1] = knockBackDistance;
        }
    }



    private int getIndex(string key)
    {
        int index = 0;
        switch (key)
        {
            case "NA":
                index = 0;
                break;
            case "SA":
                index = 1;
                break;
            case "CA":
                index = 2;
                break;

                            
            case "SS":
                index = 3;
                break;
            case "DS":
                index = 4;
                break;
            case "US":
                index = 5;
                break;


            case "NB":
                index = 6;
                break;
            case "SB":
                index = 7;
                break;
            case "DB":
                index = 8;
                break;
            case "UB":
                index = 9;
                break;


            case "AN":
                index = 10;
                break;
            case "AF":
                index = 11;
                break;
            case "AB":
                index = 12;
                break;
            case "AD":
                index = 13;
                break;
            case "AU":
                index = 14;
                break;


        }
        return index;
    }



    private int retrieveInt(int intLength, bool skipSpace)
    {
        int solution = retrieveInt(intLength);
        if (skipSpace) skipSpaces(1);
        return solution;
    }

    private int retrieveInt(int intLength)
    {
        int solution = 0;
        char[] arr = new char[intLength];
        srLine.Read(arr, 0, intLength);

        for (int i = 0; i < intLength; i++)
        {
            solution = (solution * 10) + (int)char.GetNumericValue(arr[i]);
        }
        return solution;
    }

    private string retrieve2CharKey()
    {
        char[] arr = new char[2];
        srLine.Read(arr, 0, 2);
        return (arr[0] + "" + arr[1]);
    }



    private void getNetxtLine()
    {
        // TODO                         XXXXXX
        srLine = new StringReader(srText.ReadLine());
    }

    private void skipSpaces(int num)
    {
        char[] spaces = new char[num];
        srLine.Read(spaces, 0, num);
    }



}
