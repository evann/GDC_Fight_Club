﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class MenuPointerTest : MonoBehaviour {

    public string inputType;
    public int playerNumber;

    ControllerInterface controller;

    bool isActivated = true;


    int totalCharacters = 7;
    int charactersPerLine = 4;
    int currentPosition = 0;

    Vector3 startingPos;
    float positinIncrementX = 3.45f;
    float positinIncrementY = -4.04f;

    // Use this for initialization
    void Start () {
        controller = new ControllerInterface(inputType);
        startingPos = this.transform.position;
        moveToNewPosition();
    }



	// Update is called once per frame
	void Update () {
        controller.update();
        if (isActivated)
        {
            if(controller.getInput("Pressed", "d"))
            {
                currentPosition = (currentPosition+1) % (totalCharacters + 1);
                moveToNewPosition();
            }
            if (controller.getInput("Pressed", "a")) 
            {
                currentPosition = (currentPosition + totalCharacters) % (totalCharacters + 1); // totalCharacters == (totalCharacters +1) -1
                moveToNewPosition();
            }

            if (controller.getInput("Pressed", "j")) startGame();


            if (controller.getInput("Pressed", "w")) changeStocksBy(1);
            if (controller.getInput("Pressed", "s")) changeStocksBy(-1);

        }
        else if (controller.getInput("Pressed", "a") || controller.getInput("Pressed", "d") || controller.getInput("Pressed", "k")) 
        {
            isActivated = true;
        }

        if (Input.GetKeyDown("escape")) Application.Quit();

    }


    private void moveToNewPosition()
    {
        Vector3 tempPos = startingPos;
        tempPos.x += (currentPosition % charactersPerLine) * positinIncrementX;
        tempPos.y += ((int)(currentPosition / charactersPerLine)) * positinIncrementY;


        this.transform.position = tempPos;

        if (currentPosition == 0)
        {
            GameDataHolder.isPlaying[playerNumber] = false;
            GameDataHolder.charectorSelected[playerNumber] = 0;
        }
        else
        {
            GameDataHolder.isPlaying[playerNumber] = true;
            GameDataHolder.charectorSelected[playerNumber] = currentPosition - 1;
        }
    }




    private void startGame()
    {
        // Debug.Log(GameDataHolder.isPlaying[0] + " " + GameDataHolder.isPlaying[1] + " " + GameDataHolder.isPlaying[2] + " " + GameDataHolder.isPlaying[3] + " " + GameDataHolder.isPlaying[4] + " " + GameDataHolder.isPlaying[5]);
        // Debug.Log(GameDataHolder.charectorSelected[0] + " " + GameDataHolder.charectorSelected[1] + " " + GameDataHolder.charectorSelected[2] + " " + GameDataHolder.charectorSelected[3] + " " + GameDataHolder.charectorSelected[4] + " " + GameDataHolder.charectorSelected[5]);
        if (GameDataHolder.hasAPlayer()) SceneManager.LoadScene("TTEStage_V2"); // nextStageStatic
    }



    private void changeStocksBy(int change)
    {
        int maxStock = 6;
        int minStock = 1;
        Debug.Log("currStocks :"+(GameDataHolder.stocks - minStock));
        GameDataHolder.stocks = (((GameDataHolder.stocks - minStock) + change + (maxStock - minStock + 1)) % (maxStock - minStock + 1)) + minStock;
        Debug.Log("added :"+(change + (maxStock - minStock)));
        //                                                                          ^To deal with negative values
        //GameDataHolder.stocks += change;
    }

}
