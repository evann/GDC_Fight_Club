﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FighterTagOutCode : MonoBehaviour {

    public GameObject[] p1Fighters, p2Fighters, p3Fighters, p4Fighters;
    Vector2[] p1StartingPos, p2StartingPos, p3StartingPos, p4StartingPos;
    public int p1CurrentFighter, p2CurrentFighter, p3CurrentFighter, p4CurrentFighter;
    bool firstUpdate;
    bool keepP1 = true;

    public GameObject[] hMeterGO;
    private HealthMeterCode[] hMetersCode;

    bool[] BWasPressed = new bool[4];

    // Use this for initialization
    void Start () {
        BWasPressed[0] = BWasPressed[1] = BWasPressed[2] = BWasPressed[3] = false;
        hMetersCode = new HealthMeterCode[hMeterGO.Length+1];
        for (int i=0;i< hMeterGO.Length;i++) 
        {
            hMetersCode[i+1] = hMeterGO[i].GetComponent<HealthMeterCode>();
        }


        p1CurrentFighter = -1;
        p2CurrentFighter = -1;
        p3CurrentFighter = -1;
        p4CurrentFighter = -1;

        p1StartingPos = new Vector2[p1Fighters.Length];
        p2StartingPos = new Vector2[p2Fighters.Length];
        p3StartingPos = new Vector2[p3Fighters.Length];
        p4StartingPos = new Vector2[p4Fighters.Length];

        for (int i = 0; i < p1Fighters.Length; i++)
        {
            p1Fighters[i] = p1Fighters[i].transform.GetChild(0).gameObject;
            p1StartingPos[i] = p1Fighters[i].transform.position;
        }
        for (int i = 0; i < p2Fighters.Length; i++)
        {
            p2Fighters[i] = p2Fighters[i].transform.GetChild(0).gameObject;
            p2StartingPos[i] = p2Fighters[i].transform.position;
        }
        for (int i = 0; i < p3Fighters.Length; i++)
        {
            p3Fighters[i] = p3Fighters[i].transform.GetChild(0).gameObject;
            p3StartingPos[i] = p3Fighters[i].transform.position;
        }
        for (int i = 0; i < p4Fighters.Length; i++)
        {
            p4Fighters[i] = p4Fighters[i].transform.GetChild(0).gameObject;
            p4StartingPos[i] = p4Fighters[i].transform.position;
        }

        firstUpdate = true;
    }

    // Update is called once per frame
    void Update() {

        if (firstUpdate)
        {
            rotateP3Fighters();
            rotateP1Fighters();
            rotateP2Fighters();
            rotateP4Fighters();
            removeP3();
            removeP4();

            // setUpFor2Controllers();

            firstUpdate = false;
        }

        //  HERE used to turn off activation and allow for B moves
        // if (true) return;

        if (Input.GetKeyDown("1"))
        {
            rotateP1Fighters();
            if (!keepP1) removeP3();
        }
        if (Input.GetKeyDown("2") )// || Input.GetButton("01" + "BButton"))
        {
            if (!BWasPressed[1])
            {
                rotateP2Fighters();
                BWasPressed[1] = true;
            }
        }
        else BWasPressed[1] = false;
        if (Input.GetKeyDown("3") )// || Input.GetButton("02" + "BButton")) //|| Input.GetButton("02" + "BButton")
        {
            if (!BWasPressed[2])
            {
                rotateP3Fighters();
                if (!keepP1) removeP1();
                BWasPressed[2] = true;
            }
        }
        else BWasPressed[2] = false;

        if (Input.GetKeyDown("4")) removeP3();// keepP1 = !keepP1;
        // P4 Cntrolling system
        if (Input.GetKeyDown("5") )// || Input.GetButton("03" + "BButton")) //|| Input.GetButton("02" + "BButton")
        {
            if (!BWasPressed[3])
            {
                rotateP4Fighters();
                BWasPressed[3] = true;
            }
        }
        else BWasPressed[3] = false;
        if (Input.GetKeyDown("6")) removeP4();
    }

    void setUpFor2Controllers()
    {
        rotateP1Fighters();
        rotateP2Fighters();
        rotateP3Fighters();
        keepP1 = true;
        removeP1();
    }


    private void rotateP1Fighters()
    {
        // Debug.Log("rotateP1Fighters was called");
        p1CurrentFighter = (p1CurrentFighter + 1) % p1Fighters.Length;
        for (int i = 0; i < p1Fighters.Length; i++)
        {
            if (i == p1CurrentFighter)
            {
                p1Fighters[i].transform.parent.gameObject.SetActive(true);
                
                p1Fighters[i].transform.position = StageManagerGDCFC.respawnLocation;
                hMetersCode[1].selfCF = p1Fighters[i].GetComponent<CharacterFramework>();
                hMetersCode[1].selfCF.damage = 0;
                hMetersCode[1].selfCF.ricochetParticalSystem.Stop();

                //p1Fighters[i].GetComponent<CharacterFramework>().kill();
            }
            else
            {
                p1Fighters[i].transform.position = p1StartingPos[i];
                p1Fighters[i].transform.parent.gameObject.SetActive(false);
            }
        }

        hMetersCode[1].isInUse = true;
    }

    private void rotateP2Fighters()
    {
        // Debug.Log("rotateP2Fighters was called");
        p2CurrentFighter = (p2CurrentFighter + 1) % p2Fighters.Length;
        for (int i = 0; i < p2Fighters.Length; i++)
        {
            if (i == p2CurrentFighter)
            {
                p2Fighters[i].transform.parent.gameObject.SetActive(true);
                
                p2Fighters[i].transform.position = StageManagerGDCFC.respawnLocation;
                hMetersCode[2].selfCF = p2Fighters[i].GetComponent<CharacterFramework>();
                hMetersCode[2].selfCF.damage = 0;
                hMetersCode[2].selfCF.ricochetParticalSystem.Stop();


                //p2Fighters[i].GetComponent<CharacterFramework>().kill();

            }
            else
            {
                p2Fighters[i].transform.position = p2StartingPos[i];
                p2Fighters[i].transform.parent.gameObject.SetActive(false);
            }
        }
    }


    private void rotateP3Fighters()
    {
        // Debug.Log("rotateP3Fighters was called");
        p3CurrentFighter = (p3CurrentFighter + 1) % p3Fighters.Length;
        for (int i = 0; i < p3Fighters.Length; i++)
        {
            if (i == p3CurrentFighter)
            {
                p3Fighters[i].transform.parent.gameObject.SetActive(true);

                p3Fighters[i].transform.position = StageManagerGDCFC.respawnLocation;
                hMetersCode[3].selfCF = p3Fighters[i].GetComponent<CharacterFramework>();
                hMetersCode[3].selfCF.damage = 0;
                hMetersCode[3].selfCF.ricochetParticalSystem.Stop();

                //p2Fighters[i].GetComponent<CharacterFramework>().kill();

            }
            else
            {
                p3Fighters[i].transform.position = p3StartingPos[i];
                p3Fighters[i].transform.parent.gameObject.SetActive(false);
            }
        }

        hMetersCode[3].isInUse = true;


    }

    private void rotateP4Fighters()
    {
        // Debug.Log("rotateP2Fighters was called");
        p4CurrentFighter = (p4CurrentFighter + 1) % p4Fighters.Length;
        for (int i = 0; i < p4Fighters.Length; i++)
        {
            p4Fighters[i].GetComponent<CharacterFramework>().stopRicochetParticals(); // HERE This lin should not be needed. Find a better solution. DMN
            if (i == p4CurrentFighter)
            {
                p4Fighters[i].transform.parent.gameObject.SetActive(true);

                p4Fighters[i].transform.position = StageManagerGDCFC.respawnLocation;
                // hMetersCode[4].selfCF = p4Fighters[i].GetComponent<CharacterFramework>();
                // hMetersCode[4].selfCF.damage = 0;
                // hMetersCode[4].selfCF.ricochetParticalSystem.Stop();


                //p2Fighters[i].GetComponent<CharacterFramework>().kill();

            }
            else
            {
                p4Fighters[i].transform.position = p4StartingPos[i];
                p4Fighters[i].transform.parent.gameObject.SetActive(false);
            }
        }
    }



    private void removeP1()
    {
        hMetersCode[1].isInUse = false;
        p1Fighters[p1CurrentFighter].transform.position = p1StartingPos[p1CurrentFighter];
        p1Fighters[p1CurrentFighter].transform.parent.gameObject.SetActive(false);
    }

    private void removeP3()
    {
        hMetersCode[3].isInUse = false;
        p3Fighters[p3CurrentFighter].transform.position = p3StartingPos[p3CurrentFighter];
        p3Fighters[p3CurrentFighter].transform.parent.gameObject.SetActive(false);
    }



    private void removeP4()
    {
        // hMetersCode[1].isInUse = false;
        p4Fighters[p4CurrentFighter].transform.position = p1StartingPos[p1CurrentFighter];
        p4Fighters[p4CurrentFighter].transform.parent.gameObject.SetActive(false);
    }


}
