﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestMenuStocksCode : MonoBehaviour {

    private Text readOut;

    // Use this for initialization
    void Start () {
        readOut = gameObject.GetComponent<Text>();
        readOut = GetComponent<Text>();

    }

    // Update is called once per frame
    void Update () {
        readOut.text = "Stocks: " + GameDataHolder.stocks;
	}
}
