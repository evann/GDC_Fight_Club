﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleAutoRotator : MonoBehaviour {

    ParticleSystem selfPS;
    public CharacterFramework selfCF;
    public float initalRotation;

	// Use this for initialization
	void Start () {
        selfPS = this.GetComponent<ParticleSystem>();
        if (selfCF == null) Debug.Log(" a ParticleAutoRotator is given null for CharacterFramework selfCF");
        
    }
	
	// Update is called once per frame
	void Update () {

        float result = (this.transform.eulerAngles.z + initalRotation) * selfCF.getDirectionSign() * (-1) *(Mathf.PI/180);
        selfPS.startRotation = result;

        // Debug.Log("ParticleAutoRotator.Update: " + result);

	}
}
