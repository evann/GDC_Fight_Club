﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StageManagerGDCFC : MonoBehaviour
{
    // This will be the class that will set up yur charactor's to the correct spawn points and give them the controll system they need.
    // This will also hld onto the other charactors and provide each with their "Repulsion" effect from one another.

    public bool isPlaytestStage = false;


    //Repulsion variables
    public bool spawnRandomly;
    public GameObject[] spawnPoints;
    public GameObject[] DMNTempPlayers;
    static List<GameObject> characters;
    static float maxPush = 0.8f;


    public GameObject[] healthMeters;
    public static int[] stocksRemaining;

    public static Vector2 respawnLocation;
    public Vector2 respawnLocationInput;

    public string currStage;
    public string nextStage;

    static string nextStageStatic;


    // Use this for initialization
    void Awake()
    {
        //if (respawnLocationInput == null) respawnLocation = new Vector2(0, 5.5f);
        //else
        respawnLocation = respawnLocationInput;
        characters = new List<GameObject>();

        //HERE Remove once Settings for match is implemented
        stocksRemaining = new int[6];
        for (int i = 0; i < 6; i++) { stocksRemaining[i] = GameDataHolder.stocks; }
        nextStageStatic = nextStage;

        Debug.Log("StageManagerGDCFC isPlaytestStage == " + isPlaytestStage);
        if (!isPlaytestStage)
        {
            
            startSpawning();
        }
        else
        {
            // temp DMN: Only for GameObject[] DMNTempPlayers Replace once spawning is working
            foreach (GameObject temp in DMNTempPlayers)
            {
                if (temp != null && temp.activeInHierarchy) characters.Add(temp.transform.GetChild(0).gameObject);
            }
        }
        // Update stocks

        updateCameraInfo();
    }

    private void startSpawning()
    {
        int spawnsLeft = spawnPoints.Length;

        if (spawnRandomly)
        {
            for (int i = 0; i < GameDataHolder.isPlaying.Length - 1; i++)
            {
                if (GameDataHolder.isPlaying[i])
                {
                    if (spawnsLeft <= 0)
                    {
                        Debug.LogError("NOT FATAL: NR-> StageManagerGDCFC: To may players to spawn.additional spawn points needed (random)");
                        spawnCharacterHere(spawnPoints[Random.RandomRange(0, spawnPoints.Length - 1)], i);
                    }
                    else
                    {
                        spawnCharacterHere(removeSpawnPoint(Random.RandomRange(0, spawnsLeft - 1)), i); // Give GO to given new instance of character
                        spawnsLeft--;
                    }
                }
            }
        }
        else
        {
            for (int i = 0; i < GameDataHolder.isPlaying.Length - 1; i++)
            {
                if (GameDataHolder.isPlaying[i])
                {
                    if (spawnsLeft <= 0)
                    {
                        Debug.LogError("NOT FATAL: NR-> StageManagerGDCFC: To may players to spawn.additional spawn points needed (ordered)");
                        spawnCharacterHere(spawnPoints[(spawnPoints.Length - spawnsLeft) % spawnPoints.Length], i);
                    }
                    spawnCharacterHere(spawnPoints[spawnPoints.Length - spawnsLeft], i);
                    spawnsLeft--;
                }
            }
        }
    }


    // Update is called once per frame
    void Update()
    {
        if (characters.Count == 0) Debug.LogError("NOT FATAL: NR-> StageManagerGDCFC.Update characters is empty");
        else updateCameraInfo();
        if (Input.GetKeyDown("r")) Application.LoadLevel(currStage); // Application.LoadLevel
        if (Input.GetKeyDown("n")) SceneManager.LoadScene(nextStage); // SceneManager.LoadScene
        if (Input.GetKeyDown("escape")) SceneManager.LoadScene("Test Charector Selection Menu"); //Application.Quit();

    }

    private void updateCameraInfo()
    {
        Vector2 maxPoint = new Vector2(float.NegativeInfinity, float.NegativeInfinity);
        Vector2 minPoint = new Vector2(float.PositiveInfinity, float.PositiveInfinity);

        foreach (GameObject temp in characters)
        {
            if (temp.activeInHierarchy)
            {
                if (temp.transform.position.x > maxPoint.x) maxPoint.x = temp.transform.position.x;
                if (temp.transform.position.y > maxPoint.y) maxPoint.y = temp.transform.position.y;
                if (temp.transform.position.x < minPoint.x) minPoint.x = temp.transform.position.x;
                if (temp.transform.position.y < minPoint.y) minPoint.y = temp.transform.position.y;
            }
        }


        CameraCodeGDCFC.maxPoint = maxPoint;
        CameraCodeGDCFC.minPoint = minPoint;
    }



    private GameObject removeSpawnPoint(int location)
    {
        GameObject temp = spawnPoints[location];

        for (int i = location; i < spawnPoints.Length - 1; i++)
        {
            spawnPoints[i] = spawnPoints[i + 1];
        }
        spawnPoints[spawnPoints.Length - 1] = temp;

        return temp;
    }


    private void spawnCharacterHere(GameObject locationGO, int playerNum)
    {
        // Call instatiate with locationGO's position and the character prefab as the prefab
        Debug.Log("playerNum: " + playerNum + "   charectorSelected: " + GameDataHolder.charectorSelected[playerNum]);
        GameObject tempGO = instantiatePrefab(GameDataHolder.characterPrefabs[GameDataHolder.charectorSelected[playerNum]]);
        GameObject childGO = tempGO.transform.GetChild(0).gameObject;
        CharacterFramework tempCF = childGO.GetComponent<CharacterFramework>();

        childGO.transform.position = locationGO.transform.position;
        

        // Set the color from the intended color // NOT IMPLEMENTED YET
        

        // add it to the character's list
        characters.Add(childGO);

        // set up controll scheme for the new character
        tempCF.inputType = GameDataHolder.controllScheme[playerNum];
        tempCF.playerNumber = playerNum;

        // Set up healthMeter
        if(healthMeters.Length > playerNum && healthMeters[playerNum] != null) healthMeters[playerNum].GetComponent<HealthMeterCode>().setValues(tempCF, playerNum);
    }


    private GameObject instantiatePrefab(GameObject prefab)
    {
        Vector3 pos = new Vector3(0,0,0);    // Position in space  (x,y,z)
        Quaternion rotate = Quaternion.Euler(0, 0, 0);  // Angular rotation (x,y,z)
        return Instantiate(prefab, pos, rotate);        // Create a new instance of “prefab”
                                                        //       at “pos” and at rotation “rotate”
    }






    public static Vector2 getRepulsion(GameObject self)
    {
        float repulsionDistance = 1.2f; // 0.8f

        Vector2 result = new Vector2(0, 0);
        Vector2 selfV2 = new Vector2(self.transform.position.x, self.transform.position.y);
        Vector2 tempV2 = new Vector2(0, 0);
        float tempFloat;

        if (!characters.Contains(self))
        {
            Debug.LogError("NOT FATAL: NR-> StageManagerGDCFC.getRepulsion receaved a request from an object not in the list");
            return result;
        }

        foreach (GameObject temp in characters)
        {
            if (temp != self)
            {
                tempV2 = new Vector2(temp.transform.position.x - selfV2.x, temp.transform.position.y - selfV2.y); // direction to them
                if (tempV2.magnitude < repulsionDistance)
                {
                    if (tempV2.magnitude != 0){

                    tempFloat = tempV2.magnitude;
                    tempFloat = (tempFloat - repulsionDistance) * maxPush; // directin away from them
                    tempV2 = tempV2.normalized * tempFloat;
                    result += tempV2;
                    // Debug.Log(self.transform.parent.gameObject.name + " Is close enough to " + temp.transform.parent.gameObject.name + " to be pushed by " + tempV2.magnitude);
                    }
                    else
                    {
                        tempV2 = new Vector2(Random.value, Random.value);
                        tempV2 = tempV2.normalized * repulsionDistance * maxPush;
                        result += tempV2;
                    }
                }
            }
        }

        /*
        if(Mathf.Abs(result.normalized.y) > 0.5f)
        {
            if (result.normalized.x == 0) result.x = 0.01f;
            result.x = Mathf.Sign(result.normalized.x) * (Mathf.Abs(result.y)*1.1f);
        }
        */

        result.y = result.y * 0.3f;
        // This will maximize the amount of push that can be caused by repulsion.
        if (result.magnitude > maxPush)
        {
            result = result.normalized;
            result.x = maxPush * result.x;
            result.y = maxPush * result.y;
        }

        result.y = 0;


        return result;
    }

    public static void playerDied(int playerNumber)
    {
        int stillAliveCount = 0;
        stocksRemaining[playerNumber]--;

        for(int i = 0; i < 6; i++)
        {
            if(GameDataHolder.isPlaying[i]){
                if (stocksRemaining[i] > 0) stillAliveCount++;
            }
        }

        if (stillAliveCount <= 1) 
        {
            SceneManager.LoadScene(nextStageStatic);
        }
    }



}
