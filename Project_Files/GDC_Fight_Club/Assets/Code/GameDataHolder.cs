﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameDataHolder : MonoBehaviour {

    // This is an informatin expert used pass data between scenes.
    // Main use is in Charactor selection and in passing out results of the match 

    public static GameDataHolder soloSelf;
    public static bool[] isPlaying;
    public static int[] charectorSelected;
    public static string[] controllScheme;
    public static string[,] inputConfig;
    public static int[] colorNum;
    public static int stocks;
    public static int winnersIndex;
    public static int numberfPlayers;
    public static List<TupString> matchOptions;

    public static GameObject[] characterPrefabs;


    // Use this for initialization
    void Start()
    {
        // HERE temp
        stocks = 2;
    }

    // Update is called once per frame
    void Update()
    {

    }


    void Awake()
    {
        if (soloSelf == null)
        {
            Debug.Log("GameDataHolder Awake and was first");
            soloSelf = this;
            initialize();

        }
        else
        {
            Debug.Log("GameDataHolder Awake with another iteration running");
            Destroy(this.gameObject);
            return;
        }

    }



    private void initialize()
    {
        Debug.Log("GameDataHolder init");
        DontDestroyOnLoad(this.transform.gameObject);
        isPlaying = new bool[6];
        charectorSelected = new int[6];
        controllScheme = new string[6] { "Key Board", "01", "02", "03", "04", "05" };
        inputConfig = new string[6,13]; // 4-Movement, 4-C Stick, 3-A,B,Shield, 1-jump, 1-start(pause)
        colorNum = new int[6];
        matchOptions = new List<TupString>();

        initCharacterList();

    }


    private void initALLControllSchemes()
    {
        initControllSchemes("Key Board", 0);
    }

    private void initControllSchemes(string givenInputName, int index)
    {

    }


    private void initCharacterList()
    {
        characterPrefabs = new GameObject[32];
        for (int i = 0; i < 32; i++)
        {
            // Debug.Log("init i: " + i + "||" + "characterPrefab" + (i + 1));
            characterPrefabs[i] = (GameObject)Resources.Load("characterPrefab" + (i + 1));
            // Debug.Log(characterPrefabs[i] == null);
        }
    }

    public static bool hasAPlayer()
    {
        for (int i = 0; i < 6; i++)
        {
            if (isPlaying[i])
            {
                return true;
            }
        }
        return false;
    }
}





public class TupString{

    public string a, b;

    TupString()
    {


    }

    TupString(string givenA, string givenB)
    {
        a = givenA;
        b = givenB;

    }
    

}