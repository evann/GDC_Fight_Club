﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System; // [Serializable]


public class CharacterFramework : MonoBehaviour
{

    public bool isPunchingBag;
    public int playerNumber;

    // Charactor Qualities Related Values
    public float weightType = 1; // 0 = smallest,  1 = Medium,  2 = Largest  NOT IMPLEMENTED YET
    protected float charactorRicochetModifier = 1;

    public string inputType; // Key Board
    public ControllerInterface cInterface;  // public for testing controller code

    // Attack Related Values
    public Attack currAttack;
    public bool cantCancelAction;
    public bool cantMove;
    protected int resetAllAnimationAttackBools;
    protected int resetAllOtherAnimationBools;

    // Hit Box Related Values
    public List<float> endTimesFromAttackID = new List<float>();
    public List<AttackIDPair> attackIDList = new List<AttackIDPair>();

    // Ricochet Related Values
    bool isRicocheting;
    int ricochetCount = 0;
    float minRicochetCutOff = 2f;       // damageModifier = Mathf.Pow((damage + 10) / 60, 0.38f);
    float maxRicochetCutOff = 113;      // if (ricochetPower < ricochetCutOff) isRicocheting = false;
    float ricochetCutOff;               // ricochetCutOff = ~Max( ~Min(  ricochetPower * cutOffEndPercent,   maxRicochetCutOff), minRicochetCutOff);
    float ricochetPower;
    float ricoshetDepleationRate = 45f; // ricochetPower -= ricoshetDepleationRate * Time.deltaTime
    float cutOffEndPercent = 1f / 4f;
    public ParticleSystem ricochetParticalSystem;
    protected ParticleSystem.EmissionModule ricochetEmissions;

    // Action Related Values [HERE] --- Usefull for making your character ---  
    float directionalTimeEnd = 0;
    float timeSpacing = 0.1f;
    string lastDirectionalString;
    float currentMove;
    protected bool inAContinuousMove;
    bool isCrouched;
    // bool inAir; // unused. "!isGrounded" used in it's place
    public static float movementDelayTime = 0.04f; // 0.07f
    public static float attackInputDurationTime = 0.12f; // Here is the overlap
    public static float jumpDelayTime = 0.05f; // 0.07f



    // Physics Related Values
    bool FixUdOnGround;
    bool FixUdOGOHF; // Fixed Update's On Ground OR Half Floor
    bool FixUdOnHalfFloor;   //  bool FixUd
    bool FixUdWallWalking;
    bool FixUdGoDown;
    bool FixUdCollidingWFloor;
    bool FixUdJump;
    bool FixUdFastFall;
    public int numOfAirialJumps;
    protected int airialJumpsUsed = 0;
    protected bool upBUsed = false;
    float gScale;
    int directionSign = 1;
    bool ignorYVelocityConstraint = false; // Also reset on respawn

    // ^continued...
    public CapsuleCollider2D physicalCollider;
    protected float dropThroughTime;
    float endingTimeRunOut = 0f;


    // Animation Related Variables
    public Animator animator;
    bool isGrounded;
    bool FixUdLeftGround;


    protected bool goRight = false, goLeft = false;

    // Movement/Physics related values
    float startTime;
    float prevTime;
    Rigidbody2D selfRB;
    protected float vRunSpeed = 5.8f; // 8f, 6.4f, 5.4f
    float jumpVelocity = 10.2f; // 11.8f Old wide stage  10.2 New smaller stage
    float maxFallVel = -7.8f; // -8.5 on g=-18
    float maxFFallVel = -10.2f;
    float maxVelocityPercentChangAmoutPerSecond = .5f; //500% in 1 second 
    float accelerationPower = 2;
    float frictionConstant = 1.01f;
    bool canJump = false;
    bool lookingRight = true;
    float gravityConstantModifier;
    // float movementSpeed;


    // Combat vareables
    public float damage;
    float initialDamage;
    public Vector2 responPoint = new Vector2();
    public bool alive = true;
    bool isCountering;
    bool didDie = false;
    float endDeathImmunity;


    // Testing variables
    protected bool testingRunningAnimation = false;


    // Use this for initialization
    void Start()
    {
        StartForFramework();
    }

    protected void StartForFramework()
    {


        selfRB = this.GetComponent<Rigidbody2D>();
        animator = this.GetComponent<Animator>();

        endTimesFromAttackID = new List<float>();
        attackIDList = new List<AttackIDPair>();

        gScale = selfRB.gravityScale;
        gravityConstantModifier = Mathf.Sqrt(Mathf.Abs(Physics2D.gravity.magnitude / ((float)-18))); // Sqrt(new gravity / old gravity) // converst old velocities to new ones

        initialDamage = damage;
        isRicocheting = false;
        isCountering = false;
        if (physicalCollider == null) Debug.LogError("NOT FATAL: NR-> physicalCollider NOT Publically assigned");
        if (dropThroughTime <= 0f) dropThroughTime = 0.2f;
        currAttack = new Attack(this);

        ricochetParticalSystem.Stop();
        // var emission = ricochetParticalSystem.emission;
        // emission.enabled = false;

        ricochetEmissions = ricochetParticalSystem.emission;
        cInterface = new ControllerInterface(inputType); // if(inputType != "Key Board")
    }

    // Update is called once per frame
    void Update()
    {
        UpdateForFramework();
    }

    protected void UpdateForFramework()
    {
        /*
        float currTime = Time.time;
        float deltTime = ((currTime - prevTime) * 100);
        prevTime = currTime;
        */

        animator.SetBool("cantCancelAction", cantCancelAction);
        if (cInterface != null) cInterface.update(); ;

        if (!inAContinuousMove && !cantCancelAction)
        {
            detectAttacks();

        }
        updateLR();
        // HERE used for checking running animation
        if (testingRunningAnimation)
        {
            goLeft = false; goRight = true;
            if (this.transform.position.x > 4) this.transform.position = new Vector3(this.transform.position.x - 9f, this.transform.position.y, this.transform.position.z);
        }

        /*
        if(!isRicocheting && !isPunchingBag)
        {
            updateMotion(deltTime);
        }
        */


        animator.SetFloat("CurrVelocity", (selfRB.velocity.x / vRunSpeed) * directionSign);
        if (goRight || goLeft) animator.SetBool("goLOR", true);
        else animator.SetBool("goLOR", false);


        if (didDie)
        {
            didDie = false;
            endDeathImmunity = Time.timeSinceLevelLoad + 0.5f;
            died();
        }
    }


    void FixedUpdate()
    {
        FixedUpdateForFramework();
    }

    protected void FixedUpdateForFramework()
    {

        float currTime = Time.time;
        float deltTime = ((currTime - prevTime) * 100);
        prevTime = currTime;
        if (!isRicocheting)
        {
            updateMotion(deltTime);
        }



        Vector2 normVelocity = selfRB.velocity.normalized;

        fixUdAnimationBools();



        // Turns off collider when you choose to GoDown and are on Half floor
        if (FixUdOnHalfFloor && !FixUdOnGround && FixUdGoDown)
        {
            physicalCollider.enabled = false;
            endingTimeRunOut = Time.time + dropThroughTime;
        }
        else if (endingTimeRunOut < Time.time || FixUdOnGround)
        {
            physicalCollider.enabled = true;
            endingTimeRunOut = 0f;
        }
        // Used to stop falling through the floor
        if (endingTimeRunOut >= Time.time)
        {
            FixUdOGOHF = false;
        }



        // The Main Movement loop 
        if (isRicocheting)
        {
            // ricochet code HERE1
            normVelocity = normVelocity * ricochetPower;
            ricochetPower -= ricoshetDepleationRate * Time.deltaTime; // Needs to not die off as fast HERE (But then lasts for shorter time)
            selfRB.velocity = normVelocity;

            //ricochetEmissions.rateOverTime = (float) emmisionModifier * Mathf.Max(Mathf.Min((ricochetPower*2.5f) + 70, 150) , 80);

            if (ricochetPower < ricochetCutOff)
            {
                isRicocheting = false;
                animator.SetBool("isRicocheting", false);

                selfRB.gravityScale = gScale;
                selfRB.sharedMaterial = Resources.Load("CharacterPMat2D") as PhysicsMaterial2D;
                ricochetParticalSystem.Stop();
                // var emission = ricochetParticalSystem.emission;
                // emission.enabled = false;

                // Debug.Log("Ricochet has ended " + Time.time);
            }
        }
        //If on uneven ground that is a Half floor
        else if (FixUdOGOHF && !goLeft && !goRight) // && Input.GetKey("k") // For testing
        {
            if (isGrounded && normVelocity.y < 0 && Mathf.Abs(normVelocity.x) > 0.4f)
            {
                selfRB.velocity = new Vector2(0f, 0f);
                selfRB.gravityScale = 0;
                selfRB.angularVelocity = 0f;
                // Debug.Log("a " + normVelocity.x + " " + normVelocity.y + "    " + selfRB.velocity);
                // Debug.Log(this.transform.parent.gameObject.name + "  " + isGrounded + "    x=" + selfRB.transform.position);
            }
        }
        else if (FixUdOGOHF)
        {
            selfRB.gravityScale = 2 * gScale;
        }
        else if (FixUdWallWalking)
        {
            selfRB.gravityScale = 2 * gScale;
        }
        else if (ignorYVelocityConstraint == false)// If not on any floor (and not under attack based fall speed) fall regularly
        {
            selfRB.gravityScale = gScale;

            float tempMaxFallVel = maxFallVel;
            if (FixUdFastFall) tempMaxFallVel = maxFFallVel;

            if (selfRB.velocity.y < tempMaxFallVel + 4)
            {
                float difference = 4 - (selfRB.velocity.y - tempMaxFallVel);
                if (difference < 4)
                {
                    selfRB.velocity = new Vector2(selfRB.velocity.x, selfRB.velocity.y - (difference / 20));
                }
                else selfRB.velocity = new Vector2(selfRB.velocity.x, tempMaxFallVel);
            }
        }


        detectInAir();




        FixUdOnGround = false;
        FixUdWallWalking = false;
        FixUdOnHalfFloor = false;
        FixUdGoDown = false;
        FixUdOGOHF = false;
        FixUdCollidingWFloor = false;
        FixUdLeftGround = false;
        FixUdJump = false;
        FixUdFastFall = false;
    }


    void fixUdAnimationBools()
    {
        //Resets annimation booleans on the second Frame after the boolean is set true
        if (resetAllAnimationAttackBools == 1)
        {
            resetAAAB();
            resetAllAnimationAttackBools = 0;
        }
        else if (resetAllAnimationAttackBools > 1) resetAllAnimationAttackBools--;

        if (resetAllOtherAnimationBools == 1)
        {
            resetAOAB();
            resetAllOtherAnimationBools = 0;
        }
        else if (resetAllOtherAnimationBools > 1) resetAllOtherAnimationBools--;

    }


    void detectAttacks()
    {
        if (isPunchingBag) return;
        if (cantCancelAction) return;
        if (upBUsed) return;

        string direction = cInterface.detectDirectionalAttack();
        string attackButton = "";

        attackButton = cInterface.getAttackButton();
        /*
        if (getInput("Pressed", "j")) attackButton = "B";  // B
        if (getInput("Pressed", "k")) attackButton = "A";  // A
        if (getInput("Pressed", "l")) attackButton = "Shield";  // Shield
        */
        if (attackButton == "")
        {
            // No attack

            return;
        }
        else if (direction == "Left" || direction == "Right")
        {
            // if (grounded)
            if (attackButton == "B")
            {
                sideB();
                setResetAllAnimationAttackBools();
            }
            else if (attackButton == "A")
            {
                if (!isGrounded)
                {
                    if ((direction == "Right") == (directionSign == 1))
                    {
                        aerialForwardA();
                        setResetAllAnimationAttackBools();

                    }
                    else
                    {
                        aerialBackA();
                        setResetAllAnimationAttackBools();
                    }
                }
                else
                {
                    if ((direction == "Right") != (directionSign == 1)) changeDirection();
                    sideSmash();
                    setResetAllAnimationAttackBools();
                }
            }
            else if (attackButton == "Shield")
            {
                // HERE DMN add Shield and rolling methods
                setResetAllAnimationAttackBools();
            }
        }
        else if (direction == "Down")
        {
            // if (grounded)
            if (attackButton == "B")
            {
                downB();
                setResetAllAnimationAttackBools();
            }
            if (attackButton == "A")
            {
                if (!isGrounded)
                {
                    aerialDownA();
                    setResetAllAnimationAttackBools();
                }
                else
                {
                    downSmash();
                    setResetAllAnimationAttackBools();
                }
            }
            if (attackButton == "Shield")
            {
                // HERE DMN add Shield and rolling methods
                setResetAllAnimationAttackBools();
            }
        }
        else if (direction == "Up")
        {
            // if (grounded)
            if (attackButton == "B")
            {
                if (!upBUsed)
                {
                    StartCoroutine(corotUpBUsed());
                    upB();
                    setResetAllAnimationAttackBools();
                }
            }
            if (attackButton == "A")
            {
                if (!isGrounded)
                {
                    aerialUpA();
                    setResetAllAnimationAttackBools();
                }
                else
                {
                    upSmash();
                    setResetAllAnimationAttackBools();
                }
            }
            if (attackButton == "Shield")
            {
                // HERE DMN add Shield and rolling methods
                setResetAllAnimationAttackBools();
            }
        }
        else if (isCrouched)
        {
            // if (grounded)
            if (attackButton == "B")
            {
                downB();
                setResetAllAnimationAttackBools();
            }
            if (attackButton == "A")
            {
                if (!isGrounded)
                {
                    aerialDownA();
                    setResetAllAnimationAttackBools();
                }
                else
                {
                    crouchA();
                    setResetAllAnimationAttackBools();
                }
            }
            if (attackButton == "Shield")
            {
                // HERE DMN add Shield and rolling methods
                setResetAllAnimationAttackBools();
            }
        }
        else if (direction == "")
        {
            if (attackButton == "B")
            {
                natB();
                setResetAllAnimationAttackBools();
            }
            if (attackButton == "A")
            {
                if (!isGrounded)
                {
                    aerialNatA();
                    setResetAllAnimationAttackBools();
                }
                else
                {
                    natA();
                    setResetAllAnimationAttackBools();
                }
            }
            if (attackButton == "Shield")
            {
                // HERE DMN add Shield and rolling methods
                setResetAllAnimationAttackBools();
            }
        }
        else  // DMN
        {
            if (attackButton == "B")
            {
                natB();
                // sideSmash(); // DMN
                setResetAllAnimationAttackBools();
            }
            if (attackButton == "A")
            {
                if (!isGrounded)
                {
                    aerialNatA();
                    setResetAllAnimationAttackBools();
                }
                else
                {
                    natA();
                    setResetAllAnimationAttackBools();
                }
            }
            if (attackButton == "Shield")
            {
                // HERE DMN add Shield and rolling methods
                setResetAllAnimationAttackBools();
            }
        }
    }


    /* string detectDirectionalAttack()
    {
        string result = "";

        if (getInput("Pressed", "d")) result = "Right";
        if (getInput("Pressed", "a")) result = "Left";
        if (getInput("Pressed", "s")) result = "Down";
        if (getInput("Pressed", "w")) result = "Up";
        if (getInput("Pressed", "space")) result = "Up";

        if (result == "")
        {
            if (directionalTimeEnd > Time.time) result = lastDirectionalString;
        }
        else
        {
            directionalTimeEnd = Time.time + timeSpacing;
            lastDirectionalString = result;
        }

        return result;
    }
    */

    void detectInAir()
    {
        if (FixUdCollidingWFloor && FixUdOGOHF)
        {
            isGrounded = true;
            upBUsed = false;
        }
        else if (FixUdLeftGround) isGrounded = false;

        if (!isGrounded) animator.SetBool("inAir", true);
        else animator.SetBool("inAir", false);

        isCountering = isCountering & isGrounded; // Binary AND


        // Debug.Log(!isGrounded + " " + physicalCollider.enabled); //  + " " + selfRB.gravityScale
    }


    private IEnumerator corotUpBUsed()
    {
        yield return new WaitForSeconds(0.5f);
        upBUsed = true;
        yield break;
    }



    void updateLR()
    {
        if (isPunchingBag) return;

        if (getInput("Pressed", "d"))
        {
            goLeft = false;
            goRight = true;
        }
        if (getInput("Pressed", "a"))
        {
            goLeft = true;
            goRight = false;
        }
        if (getInput("Held", "d") && !getInput("Held", "a") && goLeft)
        {
            goLeft = false;
            goRight = true;
        }
        if (!getInput("Held", "d") && getInput("Held", "a") && goRight)
        {
            goLeft = true;
            goRight = false;
        }


        if (!getInput("Held", "d") && !getInput("Held", "a") && (goLeft || goRight))
        {
            goLeft = false;
            goRight = false;
        }
        //Debug.Log(Input.GetKey("d") + " " + Input.GetKey("a"));

        if ((getInput("Pressed", "s") || getInput("Held", "s")) && !cantMove) FixUdGoDown = true;
        if ((getInput("Pressed", "w") || getInput("Pressed", "space")) && !cantMove) FixUdJump = true;
    }


    protected virtual void natA()
    {

    }

    protected virtual void sideA()
    {

    }

    protected virtual void crouchA()
    {

    }



    protected virtual void sideSmash()
    {

    }

    protected virtual void downSmash()
    {

    }

    protected virtual void upSmash()
    {

    }



    protected virtual void natB()
    {

    }

    protected virtual void sideB()
    {

    }

    protected virtual void downB()
    {

    }

    protected virtual void upB()
    {

    }



    protected virtual void aerialNatA()
    {

    }

    protected virtual void aerialForwardA()
    {

    }

    protected virtual void aerialBackA()
    {

    }

    protected virtual void aerialDownA()
    {

    }

    protected virtual void aerialUpA()
    {

    }





    protected void setAttackValues(int givenDamage, string givenKnockBackType, string givenKnockBackDistance, float givenImmunityTimeLength, string attackName)
    {
        animator.SetBool(attackName, true);
        currAttack.setAttack(givenDamage, givenKnockBackType, givenKnockBackDistance, givenImmunityTimeLength, attackName);
    }

    // KBack done with v2
    protected void setAttackValues(int givenDamage, Vector2 givenV2KBackType, string givenKnockBackDistance, float givenImmunityTimeLength, string attackName)
    {
        animator.SetBool(attackName, true);
        currAttack.setAttack(givenDamage, givenV2KBackType, givenKnockBackDistance, givenImmunityTimeLength, attackName);
    }



    //    -=-= All Times are relitive to the current time as the method is being called =-=-

    // Used when the projectile has code that kills the projectile automadically.
    // Assumed that prefab has a Rigidbody2D attatched to it
    protected void addProjectileProgrammed(GameObject prefab, Vector2 pos, Vector2 vel, float startTime) // The projectile is expected 
    {
        //Debug.LogError("NOT FATAL: NR-> \"addProjectileProgrammed\" is an unfinished method and returned immediatly after being called.");
        if (startTime < 0)
        {
            Debug.LogError("NOT FATAL: NR-> addProjectileProgrammed.startTime is lass than 0");
            return;
        }
        IEnumerator coroutine = corotAddProjectileProgrammed(prefab, pos, vel, startTime);
        StartCoroutine(coroutine);
    }

    // Used when the projectile does not have code to kill the projectile. The projectile has a timmed till switch added to it.
    // Assumed that prefab has a Rigidbody2D attatched to it
    protected void addProjectileNormal(GameObject prefab, Vector2 pos, Vector2 vel, float startTime, float lifeTime) // The projectile is expected 
    {
        //Debug.LogError("NOT FATAL: NR-> \"addProjectileNormal\" is an unfinished method and returned immediatly after being called.");
        if (startTime < 0)
        {
            Debug.LogError("NOT FATAL: NR-> addProjectileNormal.startTime is lass than 0");
            return;
        }
        if (lifeTime < 0)
        {
            Debug.LogError("NOT FATAL: NR-> addProjectileNormal.lifeTime is lass than 0");
            return;
        }
        IEnumerator coroutine = corotAddProjectileNormal(prefab, pos, vel, startTime, lifeTime);
        StartCoroutine(coroutine);
    }

    // Used when the projectile does not have code to kill the projectile. The projectile has a timmed till switch added to it based on given distance and velocity.
    // Assumed that prefab has a Rigidbody2D attatched to it
    protected void addProjectileNormal_ByDistance(GameObject prefab, Vector2 pos, Vector2 vel, float startTime, float distance) // The projectile is expected 
    {
        float lifeTime = distance / vel.magnitude;
        if (startTime < 0)
        {
            Debug.LogError("NOT FATAL: NR-> addProjectileNormal_ByDistance.startTime is lass than 0");
            return;
        }
        if (lifeTime < 0)
        {
            Debug.LogError("NOT FATAL: NR-> addProjectileNormal_ByDistance.lifeTime is lass than 0");
            return;
        }

        addProjectileNormal(prefab, pos, vel, startTime, lifeTime);
    }

    protected void setVelocityPoint(Vector2 vel, float startTime)
    {
        if (startTime < 0)
        {
            Debug.LogError("NOT FATAL: NR-> setVelocityPoint.startTime is lass than 0");
            return;
        }
        IEnumerator coroutine = corotSetVelocityPoint(vel, startTime);
        StartCoroutine(coroutine);
    }

    protected void setVelocityPeriod(Vector2 vel, float startTime, float endTime)
    {
        if (endTime <= startTime)
        {
            Debug.LogError("NOT FATAL: NR-> setVelocityPeriod, endTime <= startTime");
            return;
        }
        IEnumerator coroutine = corotSetVelocityPeriod(vel, startTime, endTime + Time.time);
        StartCoroutine(coroutine);
    }


    protected void setVelocityUntilGrounded(Vector2 vel, float startTime)
    {
        if (startTime < 0)
        {
            Debug.LogError("NOT FATAL: NR-> setVelocityUntilGrounded.startTime is lass than 0");
            return;
        }
        IEnumerator coroutine = corotSetVelocityUntilGrounded(vel, startTime);
        StartCoroutine(coroutine);
    }

    protected void counterAttack(float startTime, float endTime)
    {
        if (endTime <= startTime)
        {
            Debug.LogError("NOT FATAL: NR-> counterAttack, endTime <= startTime");
            return;
        }
        IEnumerator coroutine = corotCounterAttack(startTime, endTime);
        StartCoroutine(coroutine);
    }



    private IEnumerator corotAddProjectileProgrammed(GameObject prefab, Vector2 pos, Vector2 vel, float startTime)
    {
        int tempRicochetCount = ricochetCount;
        yield return new WaitForSeconds(startTime);
        if (ricochetCount != tempRicochetCount) yield break;
        GameObject tempGO = instantiatePrefab(prefab);

        // Setting values
        // Updating values by which way the player is looking.
        pos.x = pos.x * directionSign;
        vel.x = vel.x * directionSign;

        // set position
        Vector3 spawnPoint = this.transform.position;
        spawnPoint.x += pos.x;
        spawnPoint.y += pos.y;
        tempGO.transform.position = spawnPoint;

        // set velocity
        tempGO.GetComponent<Rigidbody2D>().velocity = vel;
        yield break;
    }

    private IEnumerator corotAddProjectileNormal(GameObject prefab, Vector2 pos, Vector2 vel, float startTime, float lifeTime)
    {
        int tempRicochetCount = ricochetCount;
        yield return new WaitForSeconds(startTime);
        if (ricochetCount != tempRicochetCount) yield break;
        GameObject tempGO = instantiatePrefab(prefab);

        // Add a component that will start a kill switch timer
        tempGO.AddComponent<killSwitchTimer>();
        tempGO.GetComponent<killSwitchTimer>().startTimer(lifeTime);

        // Setting values
        // Updating values by which way the player is looking.
        pos.x = pos.x * directionSign;
        vel.x = vel.x * directionSign;

        // set position
        Vector3 spawnPoint = this.transform.position;
        spawnPoint.x += pos.x;
        spawnPoint.y += pos.y;
        tempGO.transform.position = spawnPoint;

        // set velocity
        tempGO.GetComponent<Rigidbody2D>().velocity = vel;
        yield break;
    }

    private IEnumerator corotSetVelocityPoint(Vector2 vel, float startTime)
    {
        int tempRicochetCount = ricochetCount;
        yield return new WaitForSeconds(startTime);
        if (ricochetCount != tempRicochetCount) yield break;
        vel.x = vel.x * directionSign;
        vel.y = vel.y * gravityConstantModifier;
        selfRB.velocity = vel;
        yield break;
    }

    private IEnumerator corotSetVelocityPeriod(Vector2 vel, float startTime, float endTime)
    {
        int tempRicochetCount = ricochetCount;
        yield return new WaitForSeconds(startTime);
        vel.x = vel.x * directionSign;
        vel.y = vel.y * gravityConstantModifier;
        ignorYVelocityConstraint = true;
        while (Time.time <= endTime && ignorYVelocityConstraint == true)
        {
            if (ricochetCount != tempRicochetCount) yield break;
            selfRB.velocity = vel;
            yield return null;
        }
        ignorYVelocityConstraint = false;
        yield break;
    }

    private IEnumerator corotSetVelocityUntilGrounded(Vector2 vel, float startTime)
    {
        int tempRicochetCount = ricochetCount;
        yield return new WaitForSeconds(startTime);
        vel.x = vel.x * directionSign;
        vel.y = vel.y * gravityConstantModifier;
        ignorYVelocityConstraint = true;
        while (!isGrounded && ignorYVelocityConstraint == true)
        {
            if (ricochetCount != tempRicochetCount) yield break;
            selfRB.velocity = vel;
            yield return null;
        }
        ignorYVelocityConstraint = false;
        yield break;
    }


    private IEnumerator corotCounterAttack(float startTime, float endTime)
    {
        int tempRicochetCount = ricochetCount;
        yield return new WaitForSeconds(startTime);
        if (ricochetCount != tempRicochetCount) yield break;
        isCountering = true;
        yield return new WaitForSeconds(endTime - startTime);
        isCountering = false;
        yield break;
    }






    protected virtual void setResetAllAnimationAttackBools()
    {
        resetAllAnimationAttackBools = 2;
    }

    protected virtual void setResetAllOtherAnimationBools()
    {
        resetAllOtherAnimationBools = 2;
    }

    protected virtual void continuousAttack()
    {

    }

    void resetAAAB()
    {
        // reset All Attack Booleans
        animator.SetBool("natA", false);
        animator.SetBool("sideA", false);
        animator.SetBool("crouchA", false);

        animator.SetBool("sideSmash", false);
        animator.SetBool("downSmash", false);
        animator.SetBool("upSmash", false);

        animator.SetBool("natB", false);
        animator.SetBool("sideB", false);
        animator.SetBool("downB", false);
        animator.SetBool("upB", false);

        animator.SetBool("aerialNatA", false);
        animator.SetBool("aerialForwardA", false);
        animator.SetBool("aerialBackA", false);
        animator.SetBool("aerialDownA", false);
        animator.SetBool("aerialUpA", false);

        animator.SetBool("countered", false);
    }

    void resetAOAB()
    {
        animator.SetBool("jumpped", false);
    }


    public void gotHit(Attack givenAttack, AttackBox givenAttackBox)
    {
        //Debug.Log(givenAttack.attackID.attackName);
        updateAttackList();
        if (givenAttack.attackID == null || givenAttack.attackID.attackName == null || givenAttack.attackID.entity == null) Debug.LogError("NOT FATAL: NR-> Improper Attack ID (contains null Values)");
        if (isInAttackList(givenAttack.attackID)) return;     // Will be replaced with anyContainsAttack
        addToAttackList(givenAttack);
        if (isCountering)
        {
            // HERE-gotHit
            animator.SetBool("countered", true);
            setResetAllAnimationAttackBools();
        }
        else
        {
            startRicochet(givenAttack, givenAttackBox);
            takeDamage(givenAttack, givenAttackBox);
        }
        //Debug.Log(Time.time + "meh");
    }

    void updateAttackList()
    {
        float currTime = Time.time;
        // Debug.Log(Time.time + " updateAttackList");

        while (endTimesFromAttackID.Count > 0 && currTime >= endTimesFromAttackID[0])
        {
            endTimesFromAttackID.Remove(endTimesFromAttackID[0]);
            attackIDList.Remove(attackIDList[0]);
        }


        /*
        bool tempBool = endTimesFromAttackID.Count > 0;

        while (tempBool)
        {
            if (currTime >= endTimesFromAttackID[0])
            {

                endTimesFromAttackID.Remove(endTimesFromAttackID[0]);
                attackIDList.Remove(attackIDList[0]);
                tempBool = endTimesFromAttackID.Count > 0;
            }
            else tempBool = false;
        }
        */
    }

    public bool isInAttackList(AttackIDPair givenID)
    {
        foreach (AttackIDPair tempID in attackIDList)
        {
            if (givenID.equivalsPair(tempID)) return true;
        }
        return false;
    }

    public void addToAttackList(Attack givenAttack)
    {
        float endTime = givenAttack.immunityTimeLength + Time.time;
        int i = 0;

        while (i < endTimesFromAttackID.Count && endTimesFromAttackID[i] < endTime)
        {
            i++;
        }

        attackIDList.Insert(i, givenAttack.attackID);
        endTimesFromAttackID.Insert(i, endTime);
    }

    void takeDamage(Attack givenAttack, AttackBox givenAttackBox)
    {


        float addedDamage = givenAttack.damage;
        List<string> modifiers = givenAttackBox.getModifiers();

        if (modifiers.Contains("Double") || modifiers.Contains("x2") || modifiers.Contains("X2"))
        {
            addedDamage = addedDamage * 2;
        }
        if (modifiers.Contains("Half") || modifiers.Contains("x0.5") || modifiers.Contains("X0.5"))
        {
            addedDamage = addedDamage / 2;
        }



        damage += addedDamage;
    }

    void startRicochet(Attack givenAttack, AttackBox givenAttackBox)
    {
        float damageModifier;
        Vector2 givenDirection = givenAttack.getKnockback(this.transform.position, givenAttackBox);
        selfRB.velocity = givenDirection;

        selfRB.sharedMaterial = Resources.Load("RicochetPMat2D") as PhysicsMaterial2D;
        selfRB.gravityScale = 0;
        isRicocheting = true;
        ricochetCount++;
        animator.SetBool("isRicocheting", true);

        if (givenDirection.x == 0 && givenDirection.y == 0)
        {
            ricochetPower = 6;
            ricochetCutOff = 1;
            return;
        }

        //damageModifier = Mathf.Sqrt(Mathf.Sqrt((damage + 10)/60));
        damageModifier = Mathf.Pow((damage / 100), 0.9f) + .5f; // damageModifier = Mathf.Pow((damage + 10) / 60, 0.38f);

        ricochetPower = givenDirection.magnitude * damageModifier * charactorRicochetModifier;
        ricochetCutOff = Mathf.Max(Mathf.Min(ricochetPower * cutOffEndPercent, maxRicochetCutOff), minRicochetCutOff);
        ricochetParticalSystem.Play();
        // var emission = ricochetParticalSystem.emission;
        // emission.enabled = true;

    }

    // HERE This lin should not be needed. Find a better solution. DMN
    public void stopRicochetParticals()
    {
        ricochetParticalSystem.Stop();
        // var emission = ricochetParticalSystem.emission;
        // emission.enabled = false;
    }

    public void kill()
    {
        if (endDeathImmunity < Time.timeSinceLevelLoad) didDie = true;
    }

    private void died()
    {
        // if(StageManagerGDCFC.getLifeTotal(inputType) > 1) // respawn, else game over
        // StageManagerGDCFC.characterDied(inputType);
        StageManagerGDCFC.playerDied(playerNumber);
        this.transform.position = new Vector3(StageManagerGDCFC.respawnLocation.x, StageManagerGDCFC.respawnLocation.y, this.transform.position.z);
        selfRB.velocity = new Vector2(0, 0);
        damage = initialDamage;
        ignorYVelocityConstraint = false; // Used to cause the end of vertically restricted corotines 
    }


    public void changeDirection()
    {

        lookingRight = !lookingRight;


        if (lookingRight)
        {
            this.transform.rotation = Quaternion.Euler(0, 0, 0);
            directionSign = 1;
        }
        else if (!lookingRight)
        {
            this.transform.rotation = Quaternion.Euler(0, 180, 0);
            directionSign = -1;
        }
    }


    void updateMotion(float deltTime)
    {
        Vector2 repulsionV2 = StageManagerGDCFC.getRepulsion(this.gameObject);
        if (isPunchingBag)
        {
            selfRB.velocity = selfRB.velocity + (repulsionV2 * 1.2f); // 1.5f
            return;
        }

        float turnAroundSpeed = .08f;

        float nextRightV = selfRB.velocity.x;
        float currUpV = selfRB.velocity.y;
        //bool goingRight = nextRightV > 0;

        frictionConstant = (1 + (3 * deltTime / 100));

        if (goLeft && !FixUdWallWalking && !cantMove)
        {
            if (lookingRight == true) changeDirection();
            if (nextRightV < 0)
            {
                /*
                float xOfRight = Mathf.Abs(nextRightV / vRunMultiplyer);
                xOfRight = 1 - Mathf.Sqrt(1 - (xOfRight * xOfRight));
                xOfRight = Mathf.Min(xOfRight + (deltTime * maxVelocityPercentChangAmoutPerSecond), 1f);
                xOfRight = Mathf.Sqrt(1 - ((xOfRight - 1) * (xOfRight - 1)));
                */
                nextRightV = accelerationOfMovement(deltTime, nextRightV) * vRunSpeed * -1;
            }
            else // Going wrong direction
            {
                nextRightV -= turnAroundSpeed * vRunSpeed * deltTime;
            }
        }
        else if (goRight && !FixUdWallWalking && !cantMove)
        {
            if (lookingRight == false) changeDirection();
            if (nextRightV > 0)
            {
                /*
                float xOfRight = Mathf.Abs(nextRightV / vRunMultiplyer);
                xOfRight = 1 - Mathf.Sqrt(1 - (xOfRight * xOfRight));
                xOfRight = Mathf.Min(xOfRight + (deltTime * maxVelocityPercentChangAmoutPerSecond), 1f);
                xOfRight = Mathf.Sqrt(1 - ((xOfRight - 1) * (xOfRight - 1)));
                */
                nextRightV = accelerationOfMovement(deltTime, nextRightV) * vRunSpeed;
            }
            else // Going wrong direction
            {
                nextRightV += turnAroundSpeed * vRunSpeed * deltTime;
            }
        }
        else
        {
            nextRightV = nextRightV / (1 + (frictionConstant - 1) * .8f);
        }


        if (FixUdJump && canJump && !cantMove && !upBUsed)
        {
            if (FixUdOGOHF)
            {
                currUpV = jumpVelocity * gravityConstantModifier;    //15.0f with 2 Gravity
                FixUdOGOHF = false;
                animator.SetBool("jumpped", true);
                setResetAllOtherAnimationBools();
                // Debug.Log("gravityConstantModifier: " + gravityConstantModifier + "  jumpVelocity: " + jumpVelocity);
            }
            else if (numOfAirialJumps > airialJumpsUsed)
            {
                currUpV = jumpVelocity * gravityConstantModifier;    //15.0f with 2 Gravity
                airialJumpsUsed++;
                animator.SetBool("jumpped", true);
                setResetAllOtherAnimationBools();
                // Debug.Log("gravityConstantModifier: " + gravityConstantModifier + "  jumpVelocity: " + jumpVelocity);
            }

        }

        if (getInput("Held", "s"))
        {
            currUpV -= 40 * deltTime / 100;
            isCrouched = true;
            animator.SetBool("isCrouched", true);
            FixUdFastFall = true;
        }
        else
        {
            isCrouched = false;
            animator.SetBool("isCrouched", false);
        }



        repulsionV2.x = repulsionV2.x + nextRightV;
        repulsionV2.y = currUpV; // repulsionV2.y + 

        //Debug.Log(""+ nextRightV + "   " + currUpV);
        selfRB.velocity = repulsionV2; // = new Vector2(repulsionV2.x + nextRightV, currUpV);  //  new Vector2(nextRightV, currUpV);

        //Debug.Log("currUpV: " + currUpV);
    }


    private float accelerationOfMovement(float deltTime, float currentRightVelocity)
    {
        //return accelerationOfMovementReverse(deltTime, currentRightVelocity);
        float xOfRight = Mathf.Abs(currentRightVelocity / vRunSpeed);                               // f(x) = Abs( vel / runMultiplier )
                                                                                                    //     [goes to percentage range]
        xOfRight = 1 - Mathf.Pow(1 - Mathf.Pow(xOfRight, accelerationPower), 1 / accelerationPower);                                   // f(x) = 1 - Sqrt( 1 - x^2 )
                                                                                                                                       // 
        xOfRight = Mathf.Min(xOfRight + (deltTime * maxVelocityPercentChangAmoutPerSecond), 1f);    // f(x) = Min( x + dTime*mVPCAPS , 1 )
        //if (xOfRight == 1) Debug.Log("Max speed: "+ vRunSpeed);                                                           //  deltTime (100) = 1 second
        //else Debug.Log("------");                                                                            //  mVPCAPS = .05f  [dT/mV. = .2 seconds
        xOfRight = Mathf.Pow(1 - Mathf.Pow(1 - xOfRight, accelerationPower), 1 / accelerationPower);                                 // f(x) = Sqrt( 1 - (1-x)^2 )
                                                                                                                                     // 

        return xOfRight;
    }

    private float accelerationOfMovementReverse(float deltTime, float currentRightVelocity)
    {
        float xOfRight = Mathf.Abs(currentRightVelocity / vRunSpeed);                               // f(x) = Abs( vel / runMultiplier )
                                                                                                    //     [goes to percentage range]
        xOfRight = Mathf.Sqrt(1 - (1 - (xOfRight) * (1 - xOfRight)));                               // f(x) = Sqrt( 1 - (1-x)^2 )
                                                                                                    // 
        xOfRight = Mathf.Min(xOfRight + (deltTime * maxVelocityPercentChangAmoutPerSecond), 1f);    // f(x) = Min( x + dTime*mVPCAPS , 1 )
                                                                                                    // 
        xOfRight = 1 - Mathf.Sqrt(1 - (xOfRight * xOfRight));                                       // f(x) = 1 - Sqrt( 1 - x^2 )
                                                                                                    // 

        return xOfRight;
    }

    void OnCollisionEnter2D(Collision2D collisionInfo)
    {
        collision(collisionInfo);
    }

    void OnCollisionStay2D(Collision2D collisionInfo)
    {
        collision(collisionInfo);
    }

    void collision(Collision2D Collision2D)
    {
        // Half floor will always collide but it will not be an enabled collider if you would be able to "stand" on it
        if (Collision2D.enabled && Collision2D.gameObject.tag != "Wall")
        {
            FixUdCollidingWFloor = true;
        }
        /*
        ContactPoint2D[] points;
        points = new ContactPoint2D[4]();
        collisionInfo.GetContacts(points);
        
        ContactPoint2D[] points = collisionInfo.contacts;

        string tempString = "";
        foreach (ContactPoint2D temp in points)
        {
            tempString += temp + " ";
        }
        Debug.Log(tempString);
        */
    }



    void OnTriggerEnter2D(Collider2D collisionInfo)
    {
        triggerConnected(collisionInfo);

        /*  // for testing physics
        switch (collisionInfo.gameObject.tag)
        {
            case "Untagged":
            case "HalfFloor":
            case "Wall":
                Debug.Log("has entered " + collisionInfo.gameObject.tag);
                break;
        }
        */
    }

    void OnTriggerStay2D(Collider2D collisionInfo)
    {
        triggerConnected(collisionInfo);
        //Debug.Log("-   " +collisionInfo.gameObject.tag);
    }

    void OnTriggerExit2D(Collider2D collisionInfo)
    {
        switch (collisionInfo.gameObject.tag)
        {
            case "Untagged":
            case "HalfFloor":
            case "Wall":
                FixUdLeftGround = true;
                // Debug.Log("has left " + collisionInfo.gameObject.tag);
                break;
        }
    }

    void triggerConnected(Collider2D collisionInfo)
    {
        if (collisionInfo.gameObject != null && collisionInfo.gameObject.tag == "OffScreen")
        {
            kill();
        }
        if (collisionInfo.gameObject != null && collisionInfo.gameObject.tag == "Untagged")
        {
            FixUdOnGround = true;
            FixUdOGOHF = true;
            airialJumpsUsed = 0;
        }
        if (collisionInfo.gameObject != null && collisionInfo.gameObject.tag == "HalfFloor")
        {
            FixUdOnHalfFloor = true;
            FixUdOGOHF = true;
            airialJumpsUsed = 0;
        }
        if (collisionInfo.gameObject != null && collisionInfo.gameObject.tag == "Wall")
        {
            FixUdWallWalking = true;
        }
        //TTE Code. TBR
        //Debug.Log("y");
        //if (collisionInfo.gameObject.tag != "FrictionTag") return;
        if (selfRB.velocity.y < 2) canJump = true;
        if (!goLeft && !goRight)
        {
            //Debug.Log("x");
            float nextRightV = selfRB.velocity.x;
            float currUpV = selfRB.velocity.y;
            selfRB.velocity = new Vector2(nextRightV / (frictionConstant * 1.04f), currUpV); //frictinConstant *3
        }
    }




    public int getDirectionSign()
    {
        return directionSign;
    }



    private bool getInput(string mode, string key)
    {
        return cInterface.getInput(mode, key);
    }



    private GameObject instantiatePrefab(GameObject prefab)
    {
        Vector3 pos = new Vector3(0f, 0f, 0f);      // Position in space  (x,y,z)
        Quaternion rotate = Quaternion.Euler(0, 0, 0);  // Angular rotation (x,y,z)
        return Instantiate(prefab, pos, rotate);        // Create a new instance of “prefab”
                                                        //       at “pos” and at rotation “rotate”
    }

}









[Serializable] // for testing controller code
public class ControllerInterface
{
    public bool[] pushedBools = new bool[11];   // 7 for base. 11 includes C Stick
    public bool[] heldBools = new bool[11];     // 7 for base. 11 includes C Stick
    public bool[] attackBools = new bool[11];   // 7 for base. 11 includes C Stick
    float[] startActionTime = new float[11];    // 7 for base. 11 includes C Stick
    float[] attackDurationTime = new float[11];    // 7 for base. 11 includes C Stick

    float movementDelayTime = CharacterFramework.movementDelayTime;
    float attackInputDurationTime = CharacterFramework.attackInputDurationTime;

    public string inputName; // public for testing
    string[] inputStrings;

    float leftRightSensativity = 0.2f;
    float downSensativity = 0.4f;
    float upSensativity = 0.4f;
    // float upHoldSensativity = 0.35f;




    public ControllerInterface(string givenInputName)
    {
        // pU = pD = pL = pR = pA = pB = pS = false;
        // hU = hD = hL = hR = hA = hB = hS = false;
        inputName = givenInputName;
        inputStrings = new string[5];
        setAttackDurationTimeToZero();

        switch (inputName)
        {
            case "Key Board":
                setAllCStickToFalse();
                return;
            case "Neural Net":
                inputName = "06";
                break;
            case "01":
            case "02":
            case "03":
            case "04":
            case "05":
            case "06":
                break;
            case "":
                Debug.LogError("NOT FATAL: NR-> ControllerInterface(String) Set inputName  to Key Board or 01 through 06. Can not be left blank");
                inputName = "01";
                break;
            default:
                if (inputName == null) Debug.LogError("NOT FATAL: NR-> ControllerInterface(String) Improper inputName  null");
                else Debug.LogError("NOT FATAL: NR-> ControllerInterface(String) Improper inputName " + inputName);
                return;
        }

        inputStrings[0] = inputName + "AButton";
        inputStrings[1] = inputName + "BButton";
        inputStrings[2] = inputName + "ShieldButton";
        inputStrings[3] = inputName + "LeftAnalogX";
        inputStrings[4] = inputName + "LeftAnalogY";
        /*
        inputStrings[5] = inputName + "RightAnalogX";
        inputStrings[6] = inputName + "RightAnalogY";
        */

    }

    private void setAttackDurationTimeToZero()
    {
        for (int i = 0; i < 11; i++)
        {
            attackDurationTime[i] = 0;
        }
    }


    public void update()
    {
        if (inputName != "Key Board")
        {
            updateControler();
        }
        else
        {
            updateKeyBoard();
        }
    }

    public void updateControler()
    {
        bool tempBool = false;

        for (int i = 0; i < 3; i++)
        {
            tempBool = joystickBools(i);
            pushedBools[i] = getDown(i, tempBool);
            heldBools[i] = tempBool;
        }


        for (int i = 3; i < 7; i++)
        {
            tempBool = joystickBools(i); // If the button is currently pressed
            if (tempBool)
            {
                if (float.IsPositiveInfinity(startActionTime[i])) startActionTime[i] = Time.time + movementDelayTime;
                if (startActionTime[i] <= Time.time)
                {  // After a time has passed
                    pushedBools[i] = getDown(i, tempBool);
                    heldBools[i] = tempBool;
                }
                else
                { // Before the starting time
                    pushedBools[i] = false;
                    heldBools[i] = false;
                }
            }
            else  // Once released (and when not on)
            {
                startActionTime[i] = float.PositiveInfinity;
                pushedBools[i] = false;
                heldBools[i] = false;
            }
            updateAttackBool(i, tempBool);
        }

    }


    public void updateKeyBoard()
    {
        bool tempBool = false;

        for (int i = 0; i < 3; i++)
        {
            pushedBools[i] = keyBoardPress(i);
            heldBools[i] = keyBoardHeld(i);
        }


        for (int i = 3; i < 7; i++)
        {
            tempBool = keyBoardHeld(i); // If the button is currently pressed
            if (tempBool)
            {
                if (float.IsPositiveInfinity(startActionTime[i])) startActionTime[i] = Time.time + movementDelayTime;
                if (startActionTime[i] <= Time.time)
                { // After a time has passed
                    pushedBools[i] = getDown(i, tempBool);
                    heldBools[i] = tempBool;
                }
                else
                { // Before the starting time
                    pushedBools[i] = false;
                    heldBools[i] = false;
                }
            }
            else  // Once released (and when not on)
            {
                startActionTime[i] = float.PositiveInfinity;
                pushedBools[i] = false;
                heldBools[i] = false;
            }
            updateAttackBool(i, tempBool);
        }

    }


    private void updateAttackBool(int i, bool tempBool)
    {
        // For each direction

        //If getDown {
        //     Start timer and set true
        //Else if(Timer> time) set true
        // else set false
        if (tempBool && attackDurationTime[i] == 0)
        {
            attackDurationTime[i] = Time.time + attackInputDurationTime;
            attackBools[i] = true;
        }
        else if (attackDurationTime[i] > Time.time)
        {
            attackBools[i] = true;
        }
        else
        {
            attackBools[i] = false;
            if (!tempBool)
            {
                attackDurationTime[i] = 0; // Reset when it is not held and not X time after pressed
            }
        }

    }


    // Not needed once other is working properly 
    private bool getDown(bool prevBool, bool heldBool, bool newBool)
    {
        if (prevBool || heldBool) return false;
        return newBool;
    }

    private bool getDown(int givenInt, bool newBool)
    {
        if (pushedBools[givenInt] || heldBools[givenInt]) return false;
        return newBool;
    }


    public bool getInput(string mode, string key)
    {
        if (mode == "Pressed")
        {
            // if (key == "space") key = "w";
            switch (key)
            {
                case "k":
                    return pushedBools[0];
                case "j":
                    return pushedBools[1];
                case "l":
                    return pushedBools[2];

                case "a":
                    return pushedBools[3];
                case "d":
                    return pushedBools[4];
                case "space":
                case "w":
                    return pushedBools[5];
                case "s":
                    return pushedBools[6];
                default:
                    Debug.LogError("NOT FATAL: NR-> getInput" + mode + " " + key + " is not a proper call for joysticks");
                    return false;
            }
        }
        else if (mode == "Held")
        {
            switch (key)
            {
                case "k":
                    return heldBools[0];
                case "j":
                    return heldBools[1];
                case "l":
                    return heldBools[2];

                case "a":
                    return heldBools[3];
                case "d":
                    return heldBools[4];
                case "space":
                case "w":
                    return heldBools[5];
                case "s":
                    return heldBools[6];
                default:
                    Debug.LogError("NOT FATAL: NR-> getInput" + mode + " " + key + " is not a proper call for joysticks");
                    return false;
            }
        }
        Debug.LogError("NOT FATAL: NR-> ControllerInterface.gitInput improper mode");
        return false;
    }

    public string getAttackButton()
    {
        if (pushedBools[2]) return "Shield";
        if (pushedBools[1]) return "B";
        if (pushedBools[0]) return "A";
        return "";
    }

    public string detectDirectionalAttack()
    {
        string result = "";
        float latestTime = 0;

        for (int i = 3; i < 7; i++)
        {
            if (attackBools[i] && attackDurationTime[i] > latestTime)
            {
                result = getEnumVal(i);
                latestTime = attackDurationTime[i];
            }
        }

        // Modify for  longer names
        switch (result)
        {
            case "l":
                result = "Left";
                break;
            case "r":
                result = "Right";
                break;
            case "u":
                result = "Up";
                break;
            case "d":
                result = "Down";
                break;
        }

        return result;
    }




    int getEnumVal(string givenString)
    {
        switch (givenString)
        {
            case "a":
                return 0;
            case "b":
                return 1;
            case "s":
                return 2;

            case "l":
                return 3;
            case "r":
                return 4;
            case "u":
                return 5;
            case "d":
                return 6;
            default:
                Debug.LogError("NOT FATAL: NR-> ControllerInterface.getEnumVal(string) improper string");
                return 0;
        }
    }

    string getEnumVal(int givenInt)
    {
        switch (givenInt - 1)
        {
            case -1:
                return "a";
            case 0:
                return "b";
            case 1:
                return "s";

            case 2:
                return "l";
            case 3:
                return "r";
            case 4:
                return "u";
            case 5:
                return "d";
            default:
                Debug.LogError("NOT FATAL: NR-> ControllerInterface.getEnumVal(int) improper int");
                return "a";
        }
    }


    bool joystickBools(int givenInt)
    {
        switch (givenInt)
        {
            case 0:
                return Input.GetButton(inputStrings[0]);
            case 1:
                return Input.GetButton(inputStrings[1]);
            case 2:
                return Input.GetButton(inputStrings[2]);

            case 3:
                return -leftRightSensativity > Input.GetAxis(inputStrings[3]);
            case 4:
                return leftRightSensativity < Input.GetAxis(inputStrings[3]);
            case 5:
                return -downSensativity > Input.GetAxis(inputStrings[4]);
            case 6:
                return upSensativity < Input.GetAxis(inputStrings[4]);
            default:
                Debug.LogError("NOT FATAL: NR-> ControllerInterface.joystickBools improper int");
                return false;

        }
    }


    bool keyBoardPress(int givenInt)
    {
        switch (givenInt)
        {
            case 0:
                return Input.GetKeyDown("k");
            case 1:
                return Input.GetKeyDown("j");
            case 2:
                return Input.GetKeyDown("l");

            case 3:
                return Input.GetKeyDown("a");
            case 4:
                return Input.GetKeyDown("d");
            case 5:
                return (Input.GetKeyDown("w") || Input.GetKeyDown("space"));
            case 6:
                return Input.GetKeyDown("s");
            default:
                Debug.LogError("NOT FATAL: NR-> ControllerInterface.keyBoardBools improper int");
                return false;
        }
    }

    bool keyBoardHeld(int givenInt)
    {
        switch (givenInt)
        {
            case 0:
                return Input.GetKey("k");
            case 1:
                return Input.GetKey("j");
            case 2:
                return Input.GetKey("l");

            case 3:
                return Input.GetKey("a");
            case 4:
                return Input.GetKey("d");
            case 5:
                return (Input.GetKey("w") || Input.GetKey("space"));
            case 6:
                return Input.GetKey("s");
            default:
                Debug.LogError("NOT FATAL: NR-> ControllerInterface.keyBoardBools improper int");
                return false;
        }
    }


    void setAllCStickToFalse()
    {
        for (int i = 7; i < 11; i++)
        {
            pushedBools[i] = false;
            heldBools[i] = false;
            attackBools[i] = false;
            startActionTime[i] = float.PositiveInfinity;
            attackDurationTime[i] = float.PositiveInfinity;
        }
    }

}




