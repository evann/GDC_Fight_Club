﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TTERockMonsterCode : CharacterFrameworkWithFileReader
{


    void Start()
    {
        StartForFramework();
        // testingRunningAnimation = true;

        attackArray = new float[,] {
            { 9, 2.5f },            //NA
            { 6, 2 },               //SA
            { 6, 2f },              //CA

            { 24, 5.1f },           //SS
            { 10, 4.5f },           //DS
            { 15, 4.2f },           //US

            { 6, 2 },               //NB
            { 6, 2 },               //SB
            { 6, 2 },               //DB
            { 26, 4.5f },           //UB

            { 18, 4.5f },           //AN
            { 18, 4.5f },           //AF
            { 12, 4.5f },           //AB
            { 15, 5.0f },           //AD
            { 18, 4.2f } };         //AU


        vRunSpeed = 5.2f;
        charactorRicochetModifier = 0.85f;
        // setAttackArrayFromDocument("RockMonsterAttackFile");
        // testingRunningAnimation = true;
    }

    /* 
Tiny 1.4f
Small 2.0f
Normal 3f
Solid 3.7f
Heavy 4.2f
Massive 4.9f
Titanic 5.6f
Knock your socks off 6.4f
Jitison those socks 12f
Thoses socks are getting their socks knocked off 22f
Knock those socks out of exsistance 39f */

    // Update is called once per frame
    void Update()
    {
        UpdateForFramework();
        // Debug.Log(Time.timeSinceLevelLoad);
    }

    void FixedUpdate()
    {
        FixedUpdateForFramework();

    }



    protected override void natA()
    {
        setAttackValues((int)attackArray[0, 0], "UUUSS", "" + attackArray[0, 1], .5f, "natA"); // LateralAway
        setVelocityPeriod(new Vector2(3.8f, 1.2f), 0.08f, 0.18f);
        // sideSmash();
    }

    protected override void sideA()
    {
        setAttackValues((int)attackArray[1, 0], "US", "" + attackArray[1, 1], .5f, "sideA"); // LateralAway
    }

    protected override void crouchA()
    {
        setAttackValues((int)attackArray[2, 0], "USS", "" + attackArray[2, 1], .5f, "crouchA");
    }



    protected override void sideSmash()
    {
        setAttackValues((int)attackArray[3, 0], "UUSSS", "" + attackArray[3, 1], .5f, "sideSmash"); // LateralAway
        setVelocityPoint(new Vector2(2.6f, 6), 0.25f);
        setVelocityPoint(new Vector2(1.5f, -8), 0.46f);

    }

    protected override void downSmash()
    {
        sideSmash();// setAttackValues((int)attackArray[4, 0], "UUUS", "" + attackArray[4, 1], .5f, "downSmash"); // LateralAway
    }

    protected override void upSmash()
    {
        setAttackValues((int)attackArray[5, 0], "UUUSS", "" + attackArray[5, 1], .5f, "upSmash"); // LateralAway
        // setVelocityPeriod(new Vector2(1.16f, 5.7f), 0.22f, 0.35f); // from TTEStanly
    }



    protected override void natB()
    {
        setAttackValues((int)attackArray[6, 0], "US", "" + attackArray[6, 1], .5f, "natB"); // LateralAway
    }

    protected override void sideB()
    {
        setAttackValues((int)attackArray[7, 0], "US", "" + attackArray[7, 1], .5f, "sideB"); // LateralAway
        //setVelocityPeriod(new Vector2(8f, 0.5f), 0.1f, 0.25f);
    }

    protected override void downB()
    {
        setAttackValues((int)attackArray[8, 0], "US", "" + attackArray[8, 1], .5f, "downB"); // LateralAway
    }

    protected override void upB()
    {
        setAttackValues((int)attackArray[9, 0], "UUUUSS", "" + attackArray[9, 1], .5f, "upB"); // 4.5 is between "Heavy" and "Massive"
        setVelocityPeriod(new Vector2(7f * 0.85f, 12f*0.85f), 0.18f, 0.06f + 0.18f);
    }



    protected override void aerialNatA()
    {
        natA();// setAttackValues((int)attackArray[10, 0], "USS", "" + attackArray[10, 1], .5f, "aerialNatA"); // LateralAway
    }

    protected override void aerialForwardA()
    {
        aerialDownA();// setAttackValues((int)attackArray[11, 0], "USSS", "" + attackArray[11, 1], .5f, "aerialForwardA"); // LateralAway
    }

    protected override void aerialBackA()
    {
        upSmash();// setAttackValues((int)attackArray[12, 0], "USV", "" + attackArray[12, 1], .5f, "aerialBackA"); // LateralAway  26
    }

    protected override void aerialDownA()
    {
        setAttackValues((int)attackArray[13, 0], "DDS", "" + attackArray[13, 1], .5f, "aerialDownA");
        setVelocityPoint(new Vector2(2.6f, 8), 0.05f);
        setVelocityUntilGrounded(new Vector2(1.7f*2.4f, (-9f)*2.4f), .33f);

    }

    protected override void aerialUpA()
    {
        upSmash();// setAttackValues((int)attackArray[14, 0], "U", "" + attackArray[14, 1], .5f, "aerialUpA");
    }


    // Defore "attackArray" implementation
    /*
    
    protected override void natA()
    {
        setAttackValues(6, "USS", "Small", .5f,"natA"); // LateralAway
        setVelocityPeriod(new Vector2(1.8f,1.5f),0.18f,0.25f);
        // sideSmash();
    }
    
    protected override void sideA()
    {
        setAttackValues(6, "US", "Small", .5f, "sideA"); // LateralAway
    }

    protected override void crouchA()
    {
        setAttackValues(4 , "US", "Small", .5f, "crouchA");
    }



    protected override void sideSmash()
    {
        setAttackValues(12, "USSSS", "Solid", .5f, "sideSmash"); // LateralAway

    }
    
    protected override void downSmash()
    {
        setAttackValues(24, "UUUS", "Solid", .5f, "downSmash"); // LateralAway
    }

    protected override void upSmash()
    {
        setAttackValues(10, "UUS", "3.9", .5f, "upSmash"); // LateralAway
        setVelocityPeriod(new Vector2(1.16f, 5.7f), 0.22f,0.35f);
    }



    protected override void natB()
    {
        setAttackValues(6, "US", "Small", .5f, "natB"); // LateralAway
    }

    protected override void sideB()
    {
        setAttackValues(6, "US", "Small", .5f, "sideB"); // LateralAway
        setVelocityPeriod(new Vector2(8f,0.5f),0.1f,0.25f);
    }

    protected override void downB()
    {
        setAttackValues(6, "US", "Small", .5f, "downB"); // LateralAway
    }

    protected override void upB()
    {
        setAttackValues(26, "UUUUSS", "4.5", .5f, "upB"); // 4.5 is between "Heavy" and "Massive"
        setVelocityPeriod(new Vector2(7f,12f),0.18f,0.045f+0.18f);
    }



    protected override void aerialNatA()
    {
        setAttackValues(18, "USS", "4.5", .5f, "aerialNatA"); // LateralAway
    }

    protected override void aerialForwardA()
    {
        setAttackValues(18, "USSS", "4.5", .5f, "aerialForwardA"); // LateralAway
    }

    protected override void aerialBackA()
    {
        setAttackValues(12, "USV", "Titanic", .5f, "aerialBackA"); // LateralAway  26
    }

    protected override void aerialDownA()
    {
        setAttackValues(22, "DDDSS", "Heavy", .5f, "aerialDownA");
    }

    protected override void aerialUpA()
    {
        setAttackValues(18, "U", "Heavy", .5f, "aerialUpA");
    }

    */


}
