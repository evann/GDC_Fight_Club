﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarterKitCode : CharacterFramework{


    void Start()
    {
        StartForFramework();
        // testingRunningAnimation = true;

    }

    // Update is called once per frame
    void Update()
    {
        UpdateForFramework();
        // Debug.Log(Time.timeSinceLevelLoad);
    }

    void FixedUpdate()
    {
        FixedUpdateForFramework();

    }



    // Defore "attackArray" implementation
    
    
    protected override void natA()
    {
        setAttackValues(6, "US", "Small", .5f,"natA"); // LateralAway
    }
    
    protected override void sideA()
    {
        setAttackValues(6, "US", "Small", .5f, "sideA"); // LateralAway
    }

    protected override void crouchA()
    {
        setAttackValues(6, "US", "Small", .5f, "crouchA");
    }



    protected override void sideSmash()
    {
        setAttackValues(6, "US", "Small", .5f, "sideSmash"); // LateralAway

    }
    
    protected override void downSmash()
    {
        setAttackValues(6, "US", "Small", .5f, "downSmash"); // LateralAway
    }

    protected override void upSmash()
    {
        setAttackValues(6, "US", "Small", .5f, "upSmash"); // LateralAway
    }



    protected override void natB()
    {
        setAttackValues(6, "US", "Small", .5f, "natB"); // LateralAway
    }

    protected override void sideB()
    {
        setAttackValues(6, "US", "Small", .5f, "sideB"); // LateralAway
    }

    protected override void downB()
    {
        setAttackValues(6, "US", "Small", .5f, "downB"); // LateralAway
    }

    protected override void upB()
    {
        setAttackValues(6, "US", "Small", .5f, "upB"); // 4.5 is between "Heavy" and "Massive"
        setVelocityPeriod(new Vector2(7f,12f),0.18f,0.063f);
    }



    protected override void aerialNatA()
    {
        setAttackValues(6, "US", "Small", .5f, "aerialNatA"); // LateralAway
    }

    protected override void aerialForwardA()
    {
        setAttackValues(6, "US", "Small", .5f, "aerialForwardA"); // LateralAway
    }

    protected override void aerialBackA()
    {
        setAttackValues(6, "US", "Small", .5f, "aerialBackA"); // LateralAway  26
    }

    protected override void aerialDownA()
    {
        setAttackValues(6, "US", "Small", .5f, "aerialDownA");
    }

    protected override void aerialUpA()
    {
        setAttackValues(6, "US", "Small", .5f, "aerialUpA");
    }

    


}

