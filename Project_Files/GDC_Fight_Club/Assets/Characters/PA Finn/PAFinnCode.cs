﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PAFinnCode : CharacterFrameworkWithFileReader
{

    // Use this for initialization

    void Start()
    {
        StartForFramework();
        // testingRunningAnimation = true;

        attackArray = new float[,] {
            { 6, 2 },           //NA
            { 6, 2 },           //SA
            { 4, 2 },           //CA

            { 15, 2 },          //SS
            { 8, 3 },           //DS
            { 16, 3.7f },       //US

            { 6, 2 },           //NB
            { 6, 2 },           //SB
            { 6, 2 },           //DB
            { 14, 2 },          //UB

            { 26, 4.2f },       //AN
            { 26, 4.2f },       //AF
            { 26, 4.2f },       //AB
            { 24, 4.9f },       //AD
            { 24, 4.9f } };     //AU



        setAttackArrayFromDocument("PAAttackFile");

    }

    // Update is called once per frame
    void Update()
    {
        UpdateForFramework();
        // Debug.Log(Time.time);
    }

    void FixedUpdate()
    {
        FixedUpdateForFramework();

    }



    protected override void natA()
    {
        setVelocityPeriod(new Vector2(1.8f,1.5f),0.18f,0.25f);
        setAttackValues((int)attackArray[0, 0], "USS", "" + attackArray[0, 1], .5f, "natA"); // LateralAway
        // sideSmash();
    }

    protected override void sideA()
    {
        setAttackValues((int)attackArray[1, 0], "US", "" + attackArray[1, 1], .5f, "sideA"); // LateralAway
    }

    protected override void crouchA()
    {
        setAttackValues((int)attackArray[2, 0], "US", "" + attackArray[2, 1], .5f, "crouchA");
    }



    protected override void sideSmash()
    {
        setVelocityPoint(new Vector2(6.4f,6f), 0.34f);
        setVelocityPoint(new Vector2(6.5f,-6.5f), 0.62f);
        setAttackValues((int)attackArray[3, 0], "DSA", "" + attackArray[3, 1], .35f, "sideSmash"); // LateralAway
        //                  USSSS  "Heavy"  This knock back is changed on the second half of the attack 

    }

    protected override void downSmash()
    {
        setAttackValues((int)attackArray[4, 0], "US", "" + attackArray[4, 1], .5f, "downSmash"); // LateralAway
    }

    protected override void upSmash()
    {
        setAttackValues((int)attackArray[5, 0], "UUS", "" + attackArray[5, 1], .5f, "upSmash"); // LateralAway
    }



    protected override void natB()
    {
        setAttackValues((int)attackArray[6, 0], "US", "" + attackArray[6, 1], .5f, "natB"); // LateralAway
    }

    protected override void sideB()
    {
        setAttackValues((int)attackArray[7, 0], "US", "" + attackArray[7, 1], .5f, "sideB"); // LateralAway
    }

    protected override void downB()
    {
        setAttackValues((int)attackArray[8, 0], "US", "" + attackArray[8, 1], .5f, "downB"); // LateralAway
    }

    protected override void upB()
    {
        /*
        setVelocityPeriod(new Vector2(-16.5f, 1), 0, 0.13f); // -9.5f * 0.82f, 7f * 0.82f
        setVelocityPoint(new Vector2(-2.0f, 9.4f), 0.13f); //  0.0f, 8.3f
        setVelocityPoint(new Vector2(6f * 1.2f, 10f * 1.2f), 0.25f); // , 0.31f
        */
        setAttackValues((int)attackArray[9, 0], "USSSS", "" + attackArray[9, 1], .5f, "upB"); // LateralAway

        setVelocityPoint(new Vector2(-9.5f * 1.05f, 7f * 0.82f), 0); // -9.5f * 0.82f, 7f * 0.82f
        setVelocityPoint(new Vector2(0.0f, 8.3f), 0.18f);
        setVelocityPoint(new Vector2(6f * 1.4f, 10f * 1.2f), 0.28f); // , 0.31f

        /*
        setVelocityPoint(new Vector2(-9.5f * 0.82f, 7f * 0.82f), 0); // -9.5f * 0.82f, 7f * 0.82f
        setVelocityPoint(new Vector2(0.0f, 8.3f), 0.12f);
        setVelocityPoint(new Vector2(6f * 1.2f, 10f * 1.2f), 0.22f); // , 0.31f
        */
    }




    protected override void aerialNatA()
    {
        setAttackValues((int)attackArray[10, 0], "US", "" + attackArray[10, 1], .5f, "aerialNatA"); // LateralAway
    }

    protected override void aerialForwardA()
    {
        setAttackValues((int)attackArray[11, 0], "US", "" + attackArray[11, 1], .5f, "aerialForwardA"); // LateralAway
    }

    protected override void aerialBackA()
    {
        setAttackValues((int)attackArray[12, 0], "US", "" + attackArray[12, 1], .5f, "aerialBackA"); // LateralAway
    }

    protected override void aerialDownA()
    {
        setAttackValues((int)attackArray[13, 0], "DDDS", "" + attackArray[13, 1], .5f, "aerialDownA"); // Massive
    }

    protected override void aerialUpA()
    {
        setAttackValues((int)attackArray[14, 0], "U", "" + attackArray[14, 1], .5f, "aerialUpA");
    }



    // Defore "attackArray" implementation
    /*
    protected override void natA()
    {
        setVelocityPeriod(new Vector2(1.8f,1.5f),0.18f,0.25f);
        setAttackValues(6, "USS", "Small", .5f, "natA"); // LateralAway
        // sideSmash();
    }

    protected override void sideA()
    {
        setAttackValues(6, "US", "Small", .5f, "sideA"); // LateralAway
    }

    protected override void crouchA()
    {
        setAttackValues(4, "US", "Small", .5f, "crouchA");
    }



    protected override void sideSmash()
    {
        setVelocityPoint(new Vector2(6.4f,6f), 0.34f);
        setVelocityPoint(new Vector2(6.5f,-6.5f), 0.62f);
        setAttackValues(15, "DSA", "Small", .35f, "sideSmash"); // LateralAway
        //                  USSSS  "Heavy"  This knock back is changed on the second half of the attack 

    }

    protected override void downSmash()
    {
        setAttackValues(8, "US", "Normal", .5f, "downSmash"); // LateralAway
    }

    protected override void upSmash()
    {
        setAttackValues(16, "UUS", "Solid", .5f, "upSmash"); // LateralAway
    }



    protected override void natB()
    {
        setAttackValues(6, "US", "Small", .5f, "natB"); // LateralAway
    }

    protected override void sideB()
    {
        setAttackValues(6, "US", "Small", .5f, "sideB"); // LateralAway
    }

    protected override void downB()
    {
        setAttackValues(6, "US", "Small", .5f, "downB"); // LateralAway
    }

    protected override void upB()
    {
        
        //setVelocityPeriod(new Vector2(-16.5f, 1), 0, 0.13f); // -9.5f * 0.82f, 7f * 0.82f
        //setVelocityPoint(new Vector2(-2.0f, 9.4f), 0.13f); //  0.0f, 8.3f
        //setVelocityPoint(new Vector2(6f * 1.2f, 10f * 1.2f), 0.25f); // , 0.31f
        
        setAttackValues(14, "USSSS", "Small", .5f, "upB"); // LateralAway

        setVelocityPoint(new Vector2(-9.5f * 1.05f, 7f * 0.82f), 0); // -9.5f * 0.82f, 7f * 0.82f
        setVelocityPoint(new Vector2(0.0f, 8.3f), 0.18f);
        setVelocityPoint(new Vector2(6f * 1.4f, 10f * 1.2f), 0.28f); // , 0.31f

        
        //setVelocityPoint(new Vector2(-9.5f * 0.82f, 7f * 0.82f), 0); // -9.5f * 0.82f, 7f * 0.82f
        //setVelocityPoint(new Vector2(0.0f, 8.3f), 0.12f);
        //setVelocityPoint(new Vector2(6f * 1.2f, 10f * 1.2f), 0.22f); // , 0.31f
        
    }




    protected override void aerialNatA()
    {
        setAttackValues(26, "US", "Heavy", .5f, "aerialNatA"); // LateralAway
    }

    protected override void aerialForwardA()
    {
        setAttackValues(26, "US", "Heavy", .5f, "aerialForwardA"); // LateralAway
    }

    protected override void aerialBackA()
    {
        setAttackValues(26, "US", "Heavy", .5f, "aerialBackA"); // LateralAway
    }

    protected override void aerialDownA()
    {
        setAttackValues(24, "DDDS", "Massive", .5f, "aerialDownA"); // Massive
    }

    protected override void aerialUpA()
    {
        setAttackValues(24, "U", "Massive", .5f, "aerialUpA");
    }
    */



}

