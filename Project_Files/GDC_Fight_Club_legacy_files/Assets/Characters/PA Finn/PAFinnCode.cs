﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PAFinnCode : CharacterFramework {

    // Use this for initialization

    void Start()
    {
        StartForFramework();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateForFramework();
        // Debug.Log(Time.time);
    }

    void FixedUpdate()
    {
        FixedUpdateForFramework();

    }



    protected override void natA()
    {
        animator.SetBool("natA", true);
        currAttack.setAttack(6, "USS", "Small", .5f, "natA"); // LateralAway
        // sideSmash();
    }

    protected override void sideA()
    {
        animator.SetBool("sideA", true);
        currAttack.setAttack(6, "US", "Small", .5f, "sideA"); // LateralAway
    }

    protected override void crouchA()
    {
        animator.SetBool("crouchA", true);
        currAttack.setAttack(4, "US", "Small", .5f, "crouchA");
    }



    protected override void sideSmash()
    {
        animator.SetBool("sideSmash", true); // sideSmash
        currAttack.setAttack(12, "USSS", "Normal", .5f, "sideSmash"); // LateralAway

    }

    protected override void downSmash()
    {
        animator.SetBool("downSmash", true);
        currAttack.setAttack(8, "US", "Normal", .5f, "downSmash"); // LateralAway
    }

    protected override void upSmash()
    {
        animator.SetBool("upSmash", true);
        currAttack.setAttack(16, "UUS", "Solid", .5f, "upSmash"); // LateralAway
    }



    protected override void natB()
    {
        animator.SetBool("natB", true);
        currAttack.setAttack(6, "US", "Small", .5f, "natB"); // LateralAway
    }

    protected override void sideB()
    {
        animator.SetBool("sideB", true);
        currAttack.setAttack(6, "US", "Small", .5f, "sideB"); // LateralAway
    }

    protected override void downB()
    {
        animator.SetBool("downB", true);
        currAttack.setAttack(6, "US", "Small", .5f, "downB"); // LateralAway
    }

    protected override void upB()
    {
        animator.SetBool("upB", true);
        currAttack.setAttack(6, "US", "Small", .5f, "upB"); // LateralAway
    }




    protected override void aerialNatA()
    {
        animator.SetBool("aerialNatA", true);
        currAttack.setAttack(26, "US", "Heavy", .5f, "aerialNatA"); // LateralAway
    }

    protected override void aerialForwardA()
    {
        animator.SetBool("aerialForwardA", true);
        currAttack.setAttack(26, "US", "Heavy", .5f, "aerialForwardA"); // LateralAway
    }

    protected override void aerialBackA()
    {
        animator.SetBool("aerialBackA", true);
        currAttack.setAttack(26, "US", "Massive", .5f, "aerialBackA"); // LateralAway
    }

    protected override void aerialDownA()
    {
        animator.SetBool("aerialDownA", true);
        currAttack.setAttack(24, "DDDS", "Massive", .5f, "aerialDownA"); // Massive
    }

    protected override void aerialUpA()
    {
        animator.SetBool("aerialUpA", true);
        currAttack.setAttack(24, "U", "Massive", .5f, "aerialUpA");
    }


}

