﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExtendedAttackBox : AttackBox {



    public bool modifier05Active;
    public string modifier05;
    public bool modifier06Active;
    public string modifier06;
    public bool modifier07Active;
    public string modifier07;
    public bool modifier08Active;
    public string modifier08;
    public bool modifier09Active;
    public string modifier09;
    public bool modifier10Active;
    public string modifier10;


    public override List<string> getModifiers()
    {
        List<string> result = base.getModifiers();

        if (modifier05Active) result.Add(modifier05);
        if (modifier06Active) result.Add(modifier06);
        if (modifier07Active) result.Add(modifier07);
        if (modifier08Active) result.Add(modifier08);
        if (modifier09Active) result.Add(modifier09);
        if (modifier10Active) result.Add(modifier10);

        return result;
    }



    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
