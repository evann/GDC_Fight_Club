﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraCodeGDCFC : MonoBehaviour {

    static public bool pauseCamera;

    public static Vector2 maxPoint, minPoint;
    Vector3 initialMaxPoint, initialMinPoint;
    Camera selfCamera;

    Vector3 initialPos;
    Vector2 initialDifference;

    Vector3 nextPos = new Vector3(0f, 0f, 0f);
    Vector3 targetPos = new Vector3(0f, 0f, 0f);

    float speed = 0.5f;

    float cameraAngle;

    float lastTime;

    float maxMovementDistance = 0.2f;


    // Use this for initialization
    void Start () {
        initialPos = this.transform.position;
        selfCamera = this.GetComponent<Camera>();
        initialMaxPoint = selfCamera.ViewportToWorldPoint(new Vector3(1, 1, this.transform.position.z * (-1)));
        initialMinPoint = selfCamera.ViewportToWorldPoint(new Vector3(0, 0, this.transform.position.z * (-1)));
        initialDifference = new Vector2(initialMaxPoint.x - initialMinPoint.x, initialMaxPoint.y - initialMinPoint.y);

        //Debug.Log(initialMaxPoint + " " + initialMinPoint);
        //Debug.Log(initialDifference);
        // updateCameraPosition();

        pauseCamera = true;
    }
	
	// Update is called once per frame
	void Update () {

    }


    void FixedUpdate()
    {
        //DMN
        /*if (pauseCamera)
        {
            pauseCamera = false;
        }
        else
        { */
            updateCameraPosition();
        // }
    }

    protected virtual void updateCameraPosition()
    {
        fixGivenBounds();

        Vector2 difference = maxPoint - minPoint;
        float percentSize = Mathf.Max(difference.x / initialDifference.x, difference.y / initialDifference.y) + 0.2f;
        float deltTime = Time.time - lastTime;
        lastTime = Time.time;

        percentSize = 1 - ((1 - percentSize) / 1.6f);

        targetPos.x = minPoint.x + (difference.x / 2);
        targetPos.y = minPoint.y + (difference.y / 2);

        targetPos.z = Mathf.Max(Mathf.Min(percentSize, 1), 0.2f) * initialPos.z;

        nextPos = smothMotion(targetPos, this.transform.position, deltTime);

        //nextPos.x = targetPos.x - nextPos.x;

        //Debug.Log(difference + " " + targetPos);
        //Debug.Log("initialDifference:" + initialDifference + " difference:" + difference);
        //Debug.Log("maxPoint:" + maxPoint + " minPoint:" + minPoint + " difference:" + difference + " percentSize:" + percentSize);



        this.transform.position = nextPos;
    }

    protected void fixGivenBounds()
    {
        maxPoint.x = Mathf.Min(maxPoint.x, initialMaxPoint.x);
        maxPoint.y = Mathf.Min(maxPoint.y, initialMaxPoint.y);
        minPoint.x = Mathf.Max(minPoint.x, initialMinPoint.x);
        minPoint.y = Mathf.Max(minPoint.y, initialMinPoint.y);
    }

    protected Vector3 smothMotion(Vector3 targetPos, Vector3 currPos, float deltTime)
    {
        Vector3 result = new Vector3(0f, 0f, 0f);
        Vector3 changInPos = targetPos - currPos;
        float percentChange = Mathf.Min(Mathf.Pow((changInPos.magnitude - (changInPos.z / 25)), 2) * speed * deltTime, 0.8f);

        result = currPos + (maximumMovement(changInPos * percentChange));

        return result;
    }


    protected Vector3 maximumMovement(Vector3 givenIn)
    {
        Vector3 result = givenIn;
        Vector2 result2D = new Vector2(givenIn.x, givenIn.y);
        float givenMagnitude = result2D.magnitude;

        if (givenMagnitude > maxMovementDistance)
        {
            result.x = (result.x / givenMagnitude)*maxMovementDistance;
            result.y = (result.y / givenMagnitude)*maxMovementDistance;
        }

        return result;
    }


}
