﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System; // [Serializable]

[Serializable]
public class Attack { // : MonoBehaviour 

    // public bool active;
    public int damage;
    public string knockBackType;
    public string knockBackDistance;
    public float immunityTimeLength;

    public AttackIDPair attackID;
    public CharacterFramework entity;





    public Attack(CharacterFramework givenEntity)
    {
        entity = givenEntity;
    }



    public Attack (int givenDamage, string givenKnockBackType, string givenKnockBackDistance, float givenImmunityTimeLength, AttackIDPair givenAttackID, CharacterFramework givenEntity) // List<AttackQualities> givenQualities,
    {
        damage = givenDamage;
        knockBackType = givenKnockBackType;
        knockBackDistance = givenKnockBackDistance;
        immunityTimeLength = givenImmunityTimeLength;
        attackID = givenAttackID;
        entity = givenEntity;
    }


    public void setAttack(int givenDamage, string givenKnockBackType, string givenKnockBackDistance, float givenImmunityTimeLength, string attackName)
    {
        damage = givenDamage;
        knockBackType = givenKnockBackType;
        knockBackDistance = givenKnockBackDistance;
        immunityTimeLength = givenImmunityTimeLength;
        attackID = new AttackIDPair(entity, attackName);
    }

    public Vector2 getKnockback(Vector3 hitCharPos)
    {
        Vector2 result = new Vector2(0,0);
        float distance = 0;
        Vector3 attackerPos = entity.gameObject.transform.position;

        Vector2 awayVector = new Vector2(hitCharPos.x - attackerPos.x, hitCharPos.y - attackerPos.y);
        awayVector = awayVector.normalized;

        switch (knockBackType)
        {
            case "Up":
                result.y = 1;
                break;
            case "Down":
                result.y = -1;
                break;
            case "Left":
                result.x = -1;
                break;
            case "Right":
                result.x = 1;
                break;
            case "Horizontal":
                break;
            case "Stay":
                return result;
            case "Away":
                result.x = awayVector.x;
                result.y = awayVector.y;
                break;
            case "LateralAway":
                result.x = awayVector.x - attackerPos.x;
                break;
            case "LateralAway+":
                result.x = awayVector.x ;
                result.y = awayVector.y/2;
                Debug.Log(result.x + " " + result.y);
                break;
            default:
                int xSign = (int)Mathf.Sign(awayVector.x);
                int ySign = (int)Mathf.Sign(awayVector.y);

                foreach (char temp in knockBackType)
                {
                    switch(temp){
                        case 'U':
                            result.y += 1;
                            break;
                        case 'D':
                            result.y += -1;
                            break;
                            /*
                        case 'L':
                            result.x += -1;
                            break;
                        case 'R':
                            result.x += 1;
                            break;
                            */
                        case 'S': // Side
                            result.x += xSign;
                            break;
                        case 'A': // Away
                            result.x += awayVector.x;
                            result.y += awayVector.y;
                            //Debug.Log("DMN Away component activated " + result);
                            break;
                        case 'V': // Vertical
                            result.y += ySign;
                            //Debug.Log("DMN Away-Vertical component activated " + result);
                            break;
                        default:
                            Debug.Log("knockBackType does not contain proper syntex");
                            break;
                    }
                    if(knockBackType.Length == 0) Debug.Log("knockBackType does not contain any characters");
                }
                break;
        }

        result = result.normalized;

        switch (knockBackDistance)
        {
            case "KnockUp":
                return new Vector2(0,0);
            // break;
            case "Tiny":
                distance = 1.4f;
                break;
            case "Small":
                distance = 2.0f;
                break;
            case "Normal":
                distance = 3f;
                break;
            case "Solid":
                distance = 3.7f;
                break;
            case "Heavy":
                distance = 4.2f;
                break;
            case "Massive":
                distance = 4.9f;
                break;
            case "Titanic":
                distance = 5.6f;
                break;
            case "Knock your socks off":
                distance = 6.4f;
                break;
            case "Jitison those socks":
                distance = 12f;
                break;
            case "Thoses socks are getting their socks knocked off":
                distance = 22f;
                break;
            case "Knock those socks out of exsistance": // obliterate both yu and your socks
                distance = 39f;
                break;
            default:
                int tempInt;
                bool tempBool = int.TryParse(knockBackDistance, out tempInt);
                if (tempBool && tempInt >= 0) 
                {
                    distance = tempInt;
                }
                else
                {
                    Debug.LogError("NOT FATAL: NR-> Distance not proporly assigned for attack");
                    distance = 3;
                }
                break;
        }
        distance = distance * 5f;

        result.x = result.x * distance;
        result.y = result.y * distance;

        return result;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
