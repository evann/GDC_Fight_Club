﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TTEMovementType : MonoBehaviour {

    //int DMNCounter=0;

    public bool goRight = false, goLeft = false;


    float startTime;
    float prevTime;
    Rigidbody2D selfRB;
    //CapsuleCollider2D selfC;
    // float vJumpMultiplyer = 8f; // not used yet
    float vRunMultiplyer = 12f; // 8f
    float maxVelocityPercentChangAmoutPerSecond = .025f; //25% change in 
    float frictinConstant = 1.01f;
    bool canJump = false;
    bool lookingRight = true;

    long attackCoolDownExpier;
    long attackDurrationExpier;
    public GameObject basicTackBoxPrefab;
    public GameObject tempTackBox;

    // Combat vareables
    public int health;
    public Vector2 homeLocation = new Vector2();
    public bool alive = true;

    public float weightType = 1; // 0 = smallest,  1 = Medium,  2 = Largest



    // Time long currTime = DateTime.Now.Ticks ;
    // Time float deltTime = (((currTime -prevTime)  / 100000) % 1000000);


    // Use this for initialization
    void Start()
    {
        startTime = Time.time;
        prevTime = startTime;
        selfRB = GetComponent<Rigidbody2D>();
        //selfC = GetComponent<CapsuleCollider2D>();
    }


    public void changeDirection()
    {
        /*
        lookingRight = !lookingRight;
        this.transform.localScale = new Vector3(this.transform.localScale.x * -1, this.transform.localScale.y, 1f);
        */
    }
    
    //EMPTY
    void FixedUpdate()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float currTime = Time.time;
        float deltTime = ((currTime - prevTime) *100);
        prevTime = currTime;

        updateLR();
        updateMotion(deltTime);

    }



    void updateLR()
    {

        if (Input.GetKeyDown("d"))
        {
            goLeft = false;
            goRight = true;
        }
        if (Input.GetKeyDown("a"))
        {
            goLeft = true;
            goRight = false;
        }
        if (Input.GetKey("d") && !Input.GetKey("a") && goLeft)
        {
            goLeft = false;
            goRight = true;
        }
        if (!Input.GetKey("d") && Input.GetKey("a") && goRight)
        {
            goLeft = true;
            goRight = false;
        }


        if (!Input.GetKey("d") && !Input.GetKey("a") && (goLeft || goRight))
        {
            goLeft = false;
            goRight = false;
        }
        //Debug.Log(Input.GetKey("d") + " " + Input.GetKey("a"));
    }



    void updateMotion(float deltTime)
    {
        float nextRightV = selfRB.velocity.x;
        float currUpV = selfRB.velocity.y;

        //if moving right and accelerating right

        // bool goingRight = nextRightV > 0;

        /*
        if (LevelMannager.goRight || LevelMannager.goLeft)
        {
            float xOfRight = Mathf.Abs(nextRightV / vRunMultiplyer);
            xOfRight = 1 - Mathf.Sqrt(1 - (xOfRight * xOfRight));
            xOfRight = Mathf.Max(xOfRight + (deltTime * maxVelocityPercentChangAmoutPerSecond), 1f);
            xOfRight = Mathf.Sqrt(1 - ((xOfRight - 1) * (xOfRight - 1)));
            nextRightV = xOfRight * vRunMultiplyer;
        }
        */

        frictinConstant = (1 + (3 * deltTime / 100));

        if (goLeft)
        {
            if (lookingRight == true) changeDirection();
            if (nextRightV < 0)
            {
                float xOfRight = Mathf.Abs(nextRightV / vRunMultiplyer);
                xOfRight = 1 - Mathf.Sqrt(1 - (xOfRight * xOfRight));
                xOfRight = Mathf.Min(xOfRight + (deltTime * maxVelocityPercentChangAmoutPerSecond), 1f);
                xOfRight = Mathf.Sqrt(1 - ((xOfRight - 1) * (xOfRight - 1)));
                nextRightV = xOfRight * vRunMultiplyer * -1;
            }
            else
            {
                nextRightV -= .08f * vRunMultiplyer * deltTime;
            }
        }
        else if (goRight)
        {
            if (lookingRight == false) changeDirection();
            if (nextRightV > 0)
            {
                float xOfRight = Mathf.Abs(nextRightV / vRunMultiplyer);
                xOfRight = 1 - Mathf.Sqrt(1 - (xOfRight * xOfRight));
                xOfRight = Mathf.Min(xOfRight + (deltTime * maxVelocityPercentChangAmoutPerSecond), 1f);
                xOfRight = Mathf.Sqrt(1 - ((xOfRight - 1) * (xOfRight - 1)));
                nextRightV = xOfRight * vRunMultiplyer;
            }
            else
            {
                nextRightV += .08f * vRunMultiplyer * deltTime;
            }
        }
        else
        {
            nextRightV = nextRightV / (1 + (frictinConstant - 1) * .8f);
        }



        /*
        if (LevelMannager.goRight)
        {
            nextRightV = -1 * vRunMultiplyer;
        }
        else if (LevelMannager.goLeft)
        {
            nextRightV = 1 * vRunMultiplyer;
        }
        else
        {
            nextRightV = nextRightV / frictinConstant;
        }
        */

        /*if (Input.GetKeyDown("c") || Input.GetKeyDown("n"))
        {
            Debug.Log("c OR n");
            shortJump = !shortJump;
        }
        */
        /*
        if (Input.GetKey("space"))
        {
            Debug.Log("TRUE -----------");
            DMNCounter = 1;
        }
        else
        {
            DMNCounter = DMNCounter+1;
            Debug.Log(Input.GetKey("space")+" " + DMNCounter);
        }
        */
        
        if ((Input.GetKeyDown("w") || Input.GetKeyDown("space")) && canJump)
        {

                currUpV = 16.5f;    //15.0f with 2 Gravity
                canJump = false;

        }
        if (Input.GetKey("s"))
        {
            currUpV -= 40 * deltTime / 100;
        }

        //Debug.Log(""+ nextRightV + "   " + currUpV);
        selfRB.velocity = new Vector2(nextRightV, currUpV);



    }

    
    void OnCollisionStay2D(Collision2D collisionInfo)
    {
        //Debug.Log("y");
        //if (collisionInfo.gameObject.tag != "FrictionTag") return;
        if (selfRB.velocity.y < 2) canJump = true;
        if (!goLeft && !goRight)
        {
            //Debug.Log("x");
            float nextRightV = selfRB.velocity.x;
            float currUpV = selfRB.velocity.y;
            selfRB.velocity = new Vector2(nextRightV / (frictinConstant * 1.04f), currUpV); //frictinConstant *3
        }
    }


}
