﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextCameraSway : MonoBehaviour {

    Vector3 startingPos;
    Vector3 maxPos;
    Vector3 constants;
    Vector3 nextPos;
    float speed = 0.4f;

    // Use this for initialization
    void Start () {
        startingPos = this.transform.position;
        maxPos = new Vector3(0.1f, 0.14f, 0f);
        constants = new Vector3(1.5f, 0f, 8f);
    }
	
	// Update is called once per frame
	void Update () {

        nextPos.x = startingPos.x + Mathf.Sin((Time.time + constants.x) * speed) * maxPos.x;
        nextPos.y = startingPos.y + Mathf.Sin((Time.time + constants.y) * speed) * maxPos.y;
        nextPos.z = startingPos.z + Mathf.Sin((Time.time + constants.z) * speed) * maxPos.z;

        this.transform.position = nextPos;
    }
}
