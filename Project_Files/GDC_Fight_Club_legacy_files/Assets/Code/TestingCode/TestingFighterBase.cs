﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestingFighterBase : FighterBase
{

	// Use this for initialization
	void Start () {
        
        
        
	}
	
	// Update is called once per frame
	void Update () {
        //Debug.Log("Update called in TestingFighterBase");
        UpdateFighterBase();

        string buttonName = "J"+1+"A";
        if (Input.GetButton(buttonName))
        {
            Debug.Log("---Button is read--");
        }


        string axesName = "J" + 2 + "A";
        if (Input.GetButton("J12A"))
        {
            Debug.Log(Input.GetAxisRaw(axesName));
        }


        foreach (string temp in Input.GetJoystickNames())
        {
            Debug.Log(temp);
        }

    }
}
