﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestingButtonTimes : MonoBehaviour {

    public Material offMat, onMat;
    Renderer selfRend;
    bool nullValues = false;


	// Use this for initialization
	void Start () {
        selfRend = this.GetComponent<Renderer>();
        nullValues = offMat == null || onMat == null || selfRend == null;

    }
	
	// Update is called once per frame
	void Update () {
        if (nullValues) return;
        if (Input.GetKey("space") || Input.GetKeyDown("space"))
        {
            selfRend.material = onMat;
        }
        else
        {
            selfRend.material = offMat;
        }

    }

}
