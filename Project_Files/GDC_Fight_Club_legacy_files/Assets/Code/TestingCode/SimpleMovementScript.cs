﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleMovementScript : MonoBehaviour {

    bool goRight = false, goLeft = false, goUp = false, goDown = false;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        updateUD();
        updateLR();
	}


    void updateUD()
    {

        if (Input.GetKeyDown("s"))
        {
            goUp = false;
            goDown = true;
        }
        if (Input.GetKeyDown("w"))
        {
            goUp = true;
            goDown = false;
        }
        if (Input.GetKey("s") && !Input.GetKey("w") && goUp)
        {
            goUp = false;
            goDown = true;
        }
        if (!Input.GetKey("s") && Input.GetKey("w") && goDown)
        {
            goUp = true;
            goDown = false;
        }


        if (!Input.GetKey("s") && !Input.GetKey("w") && (goUp || goDown))
        {
            goUp = false;
            goDown = false;
        }
    }

    
    void updateLR()
    {

        if (Input.GetKeyDown("d"))
        {
            goLeft = false;
            goRight = true;
        }
        if (Input.GetKeyDown("a"))
        {
            goLeft = true;
            goRight = false;
        }
        if (Input.GetKey("d") && !Input.GetKey("a") && goLeft)
        {
            goLeft = false;
            goRight = true;
        }
        if (!Input.GetKey("d") && Input.GetKey("a") && goRight)
        {
            goLeft = true;
            goRight = false;
        }


        if (!Input.GetKey("d") && !Input.GetKey("a") && (goLeft || goRight))
        {
            goLeft = false;
            goRight = false;
        }
    }


}
