﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System; //DateTime

public class HitBox : MonoBehaviour {

    public int hitBoxID;
    public CharacterFramework entity;



    public int boxBasedDamageMultiplier;

	// Use this for initialization
	void Start () {

        if (entity == null) Debug.Log("Rough HitBox without a CharacterFramework");
    }
	
    /* public void startCoHitBox(CharacterFramework givenEntity)
    {
        entity = givenEntity;
        foreach (HitBox temp in entity.hitBoxses)
        {
            if(temp != this && temp.hitBoxID == this.hitBoxID)
            {
                coHitBoxes.Add(temp);
            }
        }
    }
    */
	
    // Update is called once per frame
	void Update () {
        //updateAttackLists();

    }

    /*
    private void updateALLAttackLists()
    {
        foreach (HitBox temp in coHitBoxes)
        {
            temp.updateAttackLists();
        }
    }

    private void updateAttackLists()
    {
        long currTime = DateTime.Now.Ticks;
        while (expierTimesFromAttackID.Count > 0 && currTime >= expierTimesFromAttackID[0])
        {
            expierTimesFromAttackID.Remove(expierTimesFromAttackID[0]);
            attackId.Remove(attackId[0]);
        }
    }



    private bool anyContainsAttack(int attackID)
    {
        bool result = false;
        foreach (HitBox temp in coHitBoxes)
        {

            temp.updateAttackLists();
        }
        return result;
    }

    */


    void OnTriggerEnter2D(Collider2D collisionInfo)
    {
        //Debug.Log("a");
        if (collisionInfo.gameObject.tag == "AttackBox") collidedWithAttackBox(collisionInfo);
    }

    void OnTriggerStay2D(Collider2D collisionInfo)
    {
        //Debug.Log("b");
        if (collisionInfo.gameObject.tag == "AttackBox") collidedWithAttackBox(collisionInfo);
    }

    private void collidedWithAttackBox(Collider2D collisionInfo)
    {

        //Debug.Log("a");
        
        //Debug.Log("aaa");
        //updateAttackLists();                                    // Will be replace with updateALLAttackLists
        AttackBox givenAttackBox = collisionInfo.gameObject.GetComponent<AttackBox>();
        if (givenAttackBox.entity == entity) return;
        Attack givenAttack = givenAttackBox.entity.currAttack;
         /*
        if (attackId.Contains(givenAttack.attackID)) return;     // Will be replaced with anyContainsAttack
        attackId.Add(givenAttack.attackID);
        long currTime = DateTime.Now.Ticks;
        expierTimesFromAttackID.Add(givenAttack.immunityTimeLength + currTime);
         */



        // AttackBox givenPlayerTackBox = collisionInfo.gameObject.GetComponent<AttackBox>();

        // Finish damage
        entity.gotHit(givenAttack, givenAttackBox);



        // Finish Knock Back
        /*
        if (givenAttack.knockBack != null)
        {
            this.GetComponent<Rigidbody2D>().velocity += givenAttack.knockBack;
        }
        */
    }

}
