﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthMeterCode : MonoBehaviour {

    public CharacterFramework selfCF; 
    public int playerNumber;
    public bool isInUse;

    private Color32 playerColor;
    private Text readOut;
    private bool movingToNewLocation;
    private Vector2 nextPosition;
    private int nextSize;
    private float movementStartTime, movementEndTime;


    // Use this for initialization
    void Start () {

        movingToNewLocation = true;

        //if (playerColor.a >= 170)
        playerColor = getColorFromNumber(playerNumber);
        //else Debug.LogError("NOT FATAL: NR-> HealthMeterCode playerColor not NULL and playerColor not implemented yet");
        this.transform.GetChild(0).gameObject.GetComponent<Image>().color = playerColor;

        readOut = this.transform.GetChild(0).GetChild(0).gameObject.GetComponent<Text>();
        if(isInUse)readOut.text = "" + selfCF.damage;
        readOut.color = getTextColor(0);
    }
	
	// Update is called once per frame
	void Update () {
        if (isInUse)
        {
            readOut.text = "" + selfCF.damage;
            readOut.color = getTextColor(selfCF.damage);

            if (movingToNewLocation)
            {
                updatePosition();
            }
        }
        else
        {
            readOut.text = "-KO-";
        }

    }



    private void updatePosition()
    {

    }

    private Color32 getColorFromNumber(int givenNumber)
    {
        Color result = new Color32((byte)255, (byte)255, (byte)255, (byte)145);
        switch (givenNumber){
            case 1:
                result = new Color32((byte)225, (byte)251, (byte)255, (byte)145);
                break;
            case 2:
                result = new Color32((byte)230, (byte)140, (byte)127, (byte)145);
                break;
            case 3:
                result = new Color32((byte)161, (byte)230, (byte)127, (byte)145);
                break;
            default:
                Debug.LogError("NOT FATAL: NR-> HealthMeterCode playerNumber is not in the proper range");
                break;
        }
        return result;
    }

    private Color32 getTextColor(float givenDamage)
    {
        int tempNumber = Mathf.Min(((int)givenDamage)/75,2);
        float percent = 0f, invertedPercent = 100f;
        Vector3 colorValueA = new Vector3(15, 15, 15);
        Vector3 colorValueB = new Vector3(15, 15, 15);

        percent = givenDamage - (tempNumber *75)/ (100/75);
        // percent = (givenDamage - (tempNumber * 75) / 75);
        // Debug.Log(percent +", " + tempNumber);

        switch (tempNumber)
        {
            case 0:
                colorValueA = new Vector3(15, 15, 15);
                colorValueB = new Vector3(83, 27, 27);
                break;
            case 1:
                colorValueA = new Vector3(83, 27, 27);
                colorValueB = new Vector3(107, 78, 21);
                break;
            default:
                colorValueA = new Vector3(107, 64, 13);
                colorValueB = new Vector3(225, 0, 0);
                percent = percent / 3;
                break;
        }

        percent = Mathf.Max(Mathf.Min(percent, 100f), 0f);
        invertedPercent = 100 - percent;

        colorValueA.x = (colorValueA.x * invertedPercent / 100) + (colorValueB.x * percent / 100);
        colorValueA.y = (colorValueA.y * invertedPercent / 100) + (colorValueB.y * percent / 100);
        colorValueA.z = (colorValueA.z * invertedPercent / 100) + (colorValueB.z * percent / 100);



        // Debug.Log(colorValueA +" "+ givenDamage + " " + tempNumber+ " " + percent);

        Color32 result = new Color32((byte)colorValueA.x, (byte)colorValueA.y, (byte)colorValueA.z, (byte)255);

        return result;
    }


    public void setNewLocation(Vector2 givenNextPosition, int givenNextSize)
    {
        nextPosition = givenNextPosition;
    }

}
