﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackBox : MonoBehaviour {

    public int currAttackNum;
    public CharacterFramework entity;
    public bool modifier01Active;
    public string modifier01;
    public bool modifier02Active;
    public string modifier02;
    public bool modifier03Active;
    public string modifier03;
    public bool modifier04Active;
    public string modifier04;

    public virtual List<string> getModifiers()
    {
        List<string> result = new List<string>();

        if (modifier01Active) result.Add(modifier01);
        if (modifier02Active) result.Add(modifier02);
        if (modifier03Active) result.Add(modifier03);
        if (modifier04Active) result.Add(modifier04);

        return result;
    }



    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
