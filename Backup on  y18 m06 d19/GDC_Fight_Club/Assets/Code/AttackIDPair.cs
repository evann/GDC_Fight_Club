﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackIDPair {

    public CharacterFramework entity;
    public string attackName;

    public AttackIDPair()
    {

    }

    public AttackIDPair(CharacterFramework givenEntity, string givenAttackName)
    {
        entity = givenEntity;
        attackName = givenAttackName;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    public bool equivalsPair(AttackIDPair otherID)
    {
        if (otherID.entity != entity) return false;
        if (otherID.attackName != attackName) return false;
        return true;
    }
}
