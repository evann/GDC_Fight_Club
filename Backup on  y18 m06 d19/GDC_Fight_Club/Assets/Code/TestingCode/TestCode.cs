﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestCode : MonoBehaviour {

	// Use this for initialization
	void Start () {
        //testExtendedAttackBox();
        //testInsertingAList();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void testExtendedAttackBox()
    {
        string sumOfTheList = "";
        ExtendedAttackBox tempEAB = new ExtendedAttackBox();

        tempEAB.modifier01Active = true;
        tempEAB.modifier01 = "up";
        tempEAB.modifier03Active = true;
        tempEAB.modifier03 = "X2";
        tempEAB.modifier06Active = true;
        tempEAB.modifier06 = "+1.5f";

        List<string> result = tempEAB.getModifiers();

        foreach (string tempString in result)
        {
            sumOfTheList = sumOfTheList + tempString + " ";
        }

        Debug.Log(sumOfTheList);
    }


    void testInsertingAList()
    {
        List<float> tempList = new List<float>();
        tempList.Add(1.2f);
        tempList.Add(3.6f);
        tempList.Add(4.8f);
        tempList.Insert(1, 2.4f);
        tempList.Insert(4, 6.0f);

        foreach (float tempFloat in tempList)
        {
            Debug.Log(tempFloat);
        }
    }

}
