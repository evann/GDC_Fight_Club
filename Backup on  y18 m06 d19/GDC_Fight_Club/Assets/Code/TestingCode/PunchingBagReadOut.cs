﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PunchingBagReadOut : MonoBehaviour {


    public CharacterFramework selfCF;
    Text selfText;


	// Use this for initialization
	void Start () {
        selfText = this.GetComponent<Text>();
        Update();
	}
	
	// Update is called once per frame
	void Update () {
        selfText.text = "" + selfCF.damage;
    }
}
