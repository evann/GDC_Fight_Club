﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterFramework : MonoBehaviour {

    public bool isPunchingBag;

    // Charactor Qualities Related Values
    public float weightType = 1; // 0 = smallest,  1 = Medium,  2 = Largest  NOT IMPLEMENTED YET
    float charactorRicochetModifier = 1;

    // Attack Related Values
    public Attack currAttack;
    public bool cantCancelAction;
    public bool cantMove;
    protected int resetAllAnimationAttackBools;

    //Hit Box Related Values
    public List<float> endTimesFromAttackID = new List<float>();
    public List<AttackIDPair> attackIDList = new List<AttackIDPair>();

    //Ricochet Related Values
    bool isRicocheting;
    float minRicochetCutOff = 2f;
    float maxRicochetCutOff = 113;
    float ricochetCutOff;
    float ricochetPower;
    float ricoshetDepleationRate = 45f;
    float cutOffEndPercent = 1f/4f;
    public ParticleSystem ricochetParticalSystem;
    public ParticleSystem.EmissionModule ricochetEmissions;

    // Action Related Values [HERE] --- Usefull for making your character ---  
    float directionalTimeEnd = 0;
    float timeSpacing = 0.3f;
    string lastDirectionalString;
    float currentMove;
    protected bool inAContinuousMove;


    // Physics Related Values
    bool FixUdOnGround;
    bool FixUdOGOHF; // Fixed Update's On Ground OR Half Floor
    bool FixUdOnHalfFloor;   //  bool FixUd
    bool FixUdWallWalking;
    bool FixUdGoDown;
    public int numOfAirialJumps;
    public int airialJumpsUsed = 0;
    bool fixedUpdateHasEnded;
    float gScale;
    int directionSign = 1;

    // ^continued...
    public CapsuleCollider2D physicalCollider;
    public float dropThroughTime;
    float endingTimeRunOut = 0f;


    public Animator animator;


    public bool goRight = false, goLeft = false;
    

    float startTime;
    float prevTime;
    Rigidbody2D selfRB;
    CapsuleCollider2D selfC;
    float vRunMultiplyer = 8f; // 8f
    float jumpVelocity = 11.8f; // -30 -> 16.5   -25
    float maxFallVel = -8.5f;
    float maxVelocityPercentChangAmoutPerSecond = .025f; //25% change in 
    float frictinConstant = 1.01f;
    bool canJump = false;
    bool lookingRight = true;

    
    // Combat vareables
    public float damage;
    float initialDamage;
    public Vector2 responPoint = new Vector2();
    public bool alive = true;

    
    // Use this for initialization
    void Start () {
        StartForFramework();
    }
	
    protected void StartForFramework()
    {
        selfRB = this.GetComponent<Rigidbody2D>();
        animator = this.GetComponent<Animator>();

        endTimesFromAttackID = new List<float>();
        attackIDList = new List<AttackIDPair>();

        gScale = selfRB.gravityScale;
        initialDamage = damage;
        isRicocheting = false;
        if (physicalCollider == null) Debug.Log("physicalCollider NOT Publically assigned");
        if (dropThroughTime <= 0f) dropThroughTime = 0.2f;
        currAttack = new Attack(this);

        ricochetParticalSystem.Stop();
        ricochetEmissions = ricochetParticalSystem.emission;
    }

	// Update is called once per frame
	void Update () {
        UpdateForFramework();
    }

    protected void UpdateForFramework()
    {
        
        float currTime = Time.time;
        float deltTime = ((currTime - prevTime) * 100);
        prevTime = currTime;

        animator.SetBool("cantCancelAction", cantCancelAction);


        if(!inAContinuousMove && !cantCancelAction)
        { 
            detectAttacks();

        }
        updateLR();
        // DMN 
        /*
        goLeft = false; goRight = true;
        if (this.transform.position.x > 4) this.transform.position = new Vector3(this.transform.position.x - 9f, this.transform.position.y, this.transform.position.z);
        */
        if(!isRicocheting && !isPunchingBag)
        {
            updateMotion(deltTime);
        }


        fixedUpdateHasEnded = false;

        animator.SetFloat("CurrVelocity",(selfRB.velocity.x / vRunMultiplyer) * directionSign );
        if (goRight || goLeft) animator.SetBool("goLOR", true);
        else animator.SetBool("goLOR", false);
    }


    void FixedUpdate()
    {
        FixedUpdateForFramework();
    }

    protected void FixedUpdateForFramework()
    {

        Vector2 normVelocity = selfRB.velocity.normalized;

        //Resets annimation booleans on the second Frame after the boolean is set true
        if (resetAllAnimationAttackBools == 1)
        {
            resetAAAB();
            resetAllAnimationAttackBools = 0;
        } else if (resetAllAnimationAttackBools >1)
        {
            resetAllAnimationAttackBools--;
        }


        // Turns off collider when you choose to GoDown and are on Half floor
        if (FixUdOnHalfFloor && !FixUdOnGround && FixUdGoDown)
        {
            physicalCollider.enabled = false;
            endingTimeRunOut = Time.time + dropThroughTime;
        }
        else if (endingTimeRunOut < Time.time || FixUdOnGround)
        {
            physicalCollider.enabled = true;
            endingTimeRunOut = 0f;
        }
        // Used to stop falling through the floor
        if(endingTimeRunOut >= Time.time)
        {
            FixUdOGOHF = false;
        }



        // The Main Movement loop 
        if (isRicocheting)
        {
            // ricochet code HERE1
            normVelocity = normVelocity * ricochetPower;
            ricochetPower -= ricoshetDepleationRate * Time.deltaTime;
            selfRB.velocity = normVelocity;

            //ricochetEmissions.rateOverTime = (float) emmisionModifier * Mathf.Max(Mathf.Min((ricochetPower*2.5f) + 70, 150) , 80);

            if (ricochetPower < ricochetCutOff)
            {
                isRicocheting = false;
                selfRB.gravityScale = gScale;
                selfRB.sharedMaterial = Resources.Load("CharacterPMat2D") as PhysicsMaterial2D;
                ricochetParticalSystem.Stop();
                // Debug.Log("Ricochet has ended " + Time.time);
            }
        }
        //If on uneven ground that is a Half floor
        else if (FixUdOGOHF && !goLeft && !goRight) // && Input.GetKey("k") // For testing
        {
            if (normVelocity.y < 0 && Mathf.Abs(normVelocity.x) > 0.4f)
            {
                selfRB.velocity = new Vector2(0f, 0f);
                selfRB.gravityScale = 0;
                selfRB.angularVelocity = 0f;
                // Debug.Log("a " + normVelocity.x + " " + normVelocity.y + "    " + selfRB.velocity);

            }
        }
        else if (FixUdOGOHF)
        {
            selfRB.gravityScale = 2*gScale;
        }
        else if (FixUdWallWalking)
        {
            selfRB.gravityScale = 2 * gScale;
        }
        else // If nt on any floor fall regularly
        {
            selfRB.gravityScale = gScale;
            if (selfRB.velocity.y < maxFallVel + 4)
            {
                float difference = 4 - (selfRB.velocity.y - maxFallVel);
                if (difference < 4)
                {
                    selfRB.velocity = new Vector2(selfRB.velocity.x, selfRB.velocity.y - (difference/20));
                }
                else selfRB.velocity = new Vector2(selfRB.velocity.x, maxFallVel);
            }
        }





        fixedUpdateHasEnded = true;
        FixUdOnGround = false;
        FixUdWallWalking = false;
        FixUdOnHalfFloor = false;
        FixUdGoDown = false;
        FixUdOGOHF = false;
    }


    void detectAttacks()
    {
        if (isPunchingBag) return;

        string direction = detectDirectionalAttack();
        string result = "";

        if (Input.GetKeyDown("j")) result = "B";  // B
        if (Input.GetKeyDown("k")) result = "A";  // A
        if (Input.GetKeyDown("l")) result = "Shield";  // Shield

        if (result == "") return;
        if (direction == "")
        {
            if (result == "B")
            {
                natB();
                // sideSmash(); // DMN
            }
            if (result == "A")
            {
                natA();
            }
            if (result == "Shield")
            {
                // HERE DMN add Shield and rolling methods
            }
        }
        else if (direction == "Left" || direction == "Right")
        {
            // if (grounded)
            if (result == "B")
            {
                sideB();
            }
            if (result == "A")
            {
                sideSmash();
            }
            if (result == "Shield")
            {
                // HERE DMN add Shield and rolling methods
            }
        }
    }



    string detectDirectionalAttack()
    {
        string result = "";

        if (Input.GetKeyDown("d")) result = "Right";
        if (Input.GetKeyDown("a")) result = "Left";
        if (Input.GetKeyDown("s")) result = "Down";
        if (Input.GetKeyDown("w")) result = "Up";

        if(result == "")
        {
            if (directionalTimeEnd > Time.time) result = lastDirectionalString;
        }
        else
        {
            directionalTimeEnd = Time.time + timeSpacing;
            lastDirectionalString = result;
        }

        return result;
    }



    void updateLR()
    {
        if (isPunchingBag) return;

        if (Input.GetKeyDown("d"))
        {
            goLeft = false;
            goRight = true;
        }
        if (Input.GetKeyDown("a"))
        {
            goLeft = true;
            goRight = false;
        }
        if (Input.GetKey("d") && !Input.GetKey("a") && goLeft)
        {
            goLeft = false;
            goRight = true;
        }
        if (!Input.GetKey("d") && Input.GetKey("a") && goRight)
        {
            goLeft = true;
            goRight = false;
        }


        if (!Input.GetKey("d") && !Input.GetKey("a") && (goLeft || goRight))
        {
            goLeft = false;
            goRight = false;
        }
        //Debug.Log(Input.GetKey("d") + " " + Input.GetKey("a"));

        if (Input.GetKeyDown("s") || Input.GetKey("s")) FixUdGoDown = true;
    }


    protected virtual void natA()
    {

    }

    protected virtual void sideA()
    {

    }

    protected virtual void crouchA()
    {

    }



    protected virtual void sideSmash()
    {

    }

    protected virtual void downSmash()
    {

    }

    protected virtual void upSmash()
    {

    }



    protected virtual void natB()
    {

    }

    protected virtual void sideB()
    {

    }

    protected virtual void downB()
    {

    }

    protected virtual void upB()
    {

    }
    


    protected virtual void aerialNatA()
    {

    }

    protected virtual void aerialForwardA()
    {

    }

    protected virtual void aerialBackA()
    {

    }

    protected virtual void aerialDownA()
    {

    }
    


    protected virtual void setResetAllAnimationAttackBools()
    {
        resetAllAnimationAttackBools = 2;
    }

    protected virtual void continuousAttack()
    {

    }

    void resetAAAB()
    {
        animator.SetBool("natA", false);
        animator.SetBool("sideA", false);
        animator.SetBool("crouchA", false);
   
        animator.SetBool("sideSmash", false);
        animator.SetBool("downSmash", false);
        animator.SetBool("upSmash", false);

        animator.SetBool("natB", false);
        animator.SetBool("sideB", false);
        animator.SetBool("downB", false);
        animator.SetBool("upB", false);
    
        animator.SetBool("aerialNatA", false);
        animator.SetBool("aerialForwardA", false);
        animator.SetBool("aerialBackA", false);
        animator.SetBool("aerialDownA", false);
    }


        

    public void gotHit(Attack givenAttack, AttackBox givenAttackBox)
    {
        // Debug.Log(givenAttack.attackID.attackName);
        updateAttackList();
        if (givenAttack.attackID == null || givenAttack.attackID.attackName == null || givenAttack.attackID.entity == null) Debug.Log("Improper Attack ID (contains null Values)");
        if (isInAttackList(givenAttack.attackID)) return;     // Will be replaced with anyContainsAttack
        addToAttackList(givenAttack);
        startRicochet(givenAttack, givenAttackBox);
        takeDamage(givenAttack, givenAttackBox);
        // Debug.Log(Time.time + "meh");
    }

    void updateAttackList()
    {
        float currTime = Time.time;
        // Debug.Log(Time.time + " updateAttackList");

        while (endTimesFromAttackID.Count > 0 && currTime >= endTimesFromAttackID[0])
        {
            endTimesFromAttackID.Remove(endTimesFromAttackID[0]);
            attackIDList.Remove(attackIDList[0]);
        }


        /*
        bool tempBool = endTimesFromAttackID.Count > 0;

        while (tempBool)
        {
            if (currTime >= endTimesFromAttackID[0])
            {

                endTimesFromAttackID.Remove(endTimesFromAttackID[0]);
                attackIDList.Remove(attackIDList[0]);
                tempBool = endTimesFromAttackID.Count > 0;
            }
            else tempBool = false;
        }
        */
    }
    
    public bool isInAttackList(AttackIDPair givenID)
    {
        foreach (AttackIDPair tempID in attackIDList)
        {
            if (givenID.equivalsPair(tempID)) return true;
        }
        return false;
    }

    public void addToAttackList(Attack givenAttack)
    {
        float endTime = givenAttack.immunityTimeLength + Time.time;
        int i = 0;

        while (i < endTimesFromAttackID.Count && endTimesFromAttackID[i] < endTime)
        {
            i++;
        }

        attackIDList.Insert(i,givenAttack.attackID);
        endTimesFromAttackID.Insert(i,endTime);
    }

    void takeDamage(Attack givenAttack, AttackBox givenAttackBox)
    {
        float addedDamage = givenAttack.damage;
        List<string> modifiers = givenAttackBox.getModifiers();
        
        if(modifiers.Contains("Double")){
            addedDamage = addedDamage * 2;
        }
        if(modifiers.Contains("Half")){
            addedDamage = addedDamage / 2;
        }

        /*

        



        */

        damage += addedDamage;
    }

    void startRicochet(Attack givenAttack, AttackBox givenAttackBox)
    {
        float damageModifier;
        Vector2 givenDirection = givenAttack.getKnockback(this.transform.position);
        selfRB.velocity = givenDirection;

        selfRB.sharedMaterial = Resources.Load("RicochetPMat2D") as PhysicsMaterial2D;
        selfRB.gravityScale = 0;
        isRicocheting = true;

        if (givenDirection.x == 0 && givenDirection.y == 0)
        {
            ricochetPower = 6;
            ricochetCutOff = 1;
            return;
        }

        //damageModifier = Mathf.Sqrt(Mathf.Sqrt((damage + 10)/60));
        damageModifier = Mathf.Pow((damage + 10) / 60, 0.38f);

        ricochetPower = givenDirection.magnitude * damageModifier * charactorRicochetModifier;
        ricochetCutOff = Mathf.Max(Mathf.Min(ricochetPower * cutOffEndPercent, maxRicochetCutOff), minRicochetCutOff);
        ricochetParticalSystem.Play();
    }


    void died()
    {
        this.transform.position = new Vector3(StageManagerGDCFC.respawnLocation.x, StageManagerGDCFC.respawnLocation.y, this.transform.position.z);
        selfRB.velocity = new Vector2(0, 0);
        damage = initialDamage;
    }


    public void changeDirection()
    {
        
        lookingRight = !lookingRight;
        

        if (lookingRight)
        {
            this.transform.rotation = Quaternion.Euler(0,0,0);
            directionSign = 1;
        }
        else if (!lookingRight)
        {
            this.transform.rotation = Quaternion.Euler(0,180,0);
            directionSign = -1;
        }
    }


    void updateMotion(float deltTime)
    {
        if (isPunchingBag) return;
        
        float nextRightV = selfRB.velocity.x;
        float currUpV = selfRB.velocity.y;
        //bool goingRight = nextRightV > 0;

        frictinConstant = (1 + (3 * deltTime / 100));

        if (goLeft && !FixUdWallWalking)
        {
            if (lookingRight == true) changeDirection();
            if (nextRightV < 0)
            {
                float xOfRight = Mathf.Abs(nextRightV / vRunMultiplyer);
                xOfRight = 1 - Mathf.Sqrt(1 - (xOfRight * xOfRight));
                xOfRight = Mathf.Min(xOfRight + (deltTime * maxVelocityPercentChangAmoutPerSecond), 1f);
                xOfRight = Mathf.Sqrt(1 - ((xOfRight - 1) * (xOfRight - 1)));
                nextRightV = xOfRight * vRunMultiplyer * -1;
            }
            else
            {
                nextRightV -= .08f * vRunMultiplyer * deltTime;
            }
        }
        else if (goRight && !FixUdWallWalking)
        {
            if (lookingRight == false) changeDirection();
            if (nextRightV > 0)
            {
                float xOfRight = Mathf.Abs(nextRightV / vRunMultiplyer);
                xOfRight = 1 - Mathf.Sqrt(1 - (xOfRight * xOfRight));
                xOfRight = Mathf.Min(xOfRight + (deltTime * maxVelocityPercentChangAmoutPerSecond), 1f);
                xOfRight = Mathf.Sqrt(1 - ((xOfRight - 1) * (xOfRight - 1)));
                nextRightV = xOfRight * vRunMultiplyer;
            }
            else
            {
                nextRightV += .08f * vRunMultiplyer * deltTime;
            }
        }
        else
        {
            nextRightV = nextRightV / (1 + (frictinConstant - 1) * .8f);
        }

        
        /*
        if (LevelMannager.goRight)
        {
            nextRightV = -1 * vRunMultiplyer;
        }
        else if (LevelMannager.goLeft)
        {
            nextRightV = 1 * vRunMultiplyer;
        }
        else
        {
            nextRightV = nextRightV / frictinConstant;
        }
        */



        if ((Input.GetKeyDown("w") || Input.GetKeyDown("space")) && canJump)
        {
            if(FixUdOGOHF)
            {
                currUpV = jumpVelocity;    //15.0f with 2 Gravity
                FixUdOGOHF = false;
            }
            else if(numOfAirialJumps > airialJumpsUsed)
            {
                currUpV = jumpVelocity;    //15.0f with 2 Gravity
                airialJumpsUsed++;
            }

        }
        if (Input.GetKey("s"))
        {
            currUpV -= 40 * deltTime / 100;
        }

        //Debug.Log(""+ nextRightV + "   " + currUpV);
        selfRB.velocity = new Vector2(nextRightV, currUpV);
        
        
    }




    void OnCollisionEnter2D(Collision2D collisionInfo)
    {
        collision(collisionInfo);
    }

    void OnCollisionStay2D(Collision2D collisionInfo)
    {
        collision(collisionInfo);
    }

    void collision(Collision2D collisionInfo)
    {
        /*
        ContactPoint2D[] points;
        points = new ContactPoint2D[4]();
        collisionInfo.GetContacts(points);
        
        ContactPoint2D[] points = collisionInfo.contacts;

        string tempString = "";
        foreach (ContactPoint2D temp in points)
        {
            tempString += temp + " ";
        }
        Debug.Log(tempString);
        */
    }



    void OnTriggerEnter2D(Collider2D collisionInfo)
    {
        triggerConnected(collisionInfo);
    }

    void OnTriggerStay2D(Collider2D collisionInfo)
    {
        triggerConnected(collisionInfo);
    }

    void triggerConnected(Collider2D collisionInfo)
    {
        if(collisionInfo.gameObject != null && collisionInfo.gameObject.tag == "OffScreen")
        {
            died();
        }
        if (collisionInfo.gameObject != null && collisionInfo.gameObject.tag == "Untagged")
        {
            FixUdOnGround = true;
            FixUdOGOHF = true;
            airialJumpsUsed = 0;
        }
        if (collisionInfo.gameObject != null && collisionInfo.gameObject.tag == "HalfFloor")
        {
            FixUdOnHalfFloor = true;
            FixUdOGOHF = true;
            airialJumpsUsed = 0;
        }
        if (collisionInfo.gameObject != null && collisionInfo.gameObject.tag == "Wall")
        {
            FixUdWallWalking = true;
        }
        //TTE Code. TBR
        //Debug.Log("y");
        //if (collisionInfo.gameObject.tag != "FrictionTag") return;
        if (selfRB.velocity.y < 2) canJump = true;
        if (!goLeft && !goRight)
        {
            //Debug.Log("x");
            float nextRightV = selfRB.velocity.x;
            float currUpV = selfRB.velocity.y;
            selfRB.velocity = new Vector2(nextRightV / (frictinConstant * 1.04f), currUpV); //frictinConstant *3
        }
    }




    public int getDirectionSign()
    {
        return directionSign;
    }

}
