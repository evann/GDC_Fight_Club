﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TTEStanleyCode : CharacterFramework {

    

	// Use this for initialization

	void Start () {
        StartForFramework();
    }

    // Update is called once per frame
    void Update () {
        UpdateForFramework();
        // Debug.Log(Time.time);
    }

    void FixedUpdate()
    {
        FixedUpdateForFramework();

    }


    
    protected override void natA()
    {
        animator.SetBool("natA", true);
        setResetAllAnimationAttackBools();
        currAttack.setAttack(6, "US", "Small", .5f,"natA"); // LateralAway
        // sideSmash();
    }
    /*
    protected override void sideA()
    {
        animator.SetBool("sideA", true);
        setResetAllAnimationAttackBools();
    }

    protected override void crouchA()
    {
        animator.SetBool("crouchA", true);
        setResetAllAnimationAttackBools();
    }

    */

    protected override void sideSmash()
    {
        animator.SetBool("sideSmash", true); // sideSmash
        setResetAllAnimationAttackBools();
        currAttack.setAttack(12, "USSS", "Solid", .5f, "sideSmash"); // LateralAway

    }
    /*
    protected override void downSmash()
    {
        animator.SetBool("downSmash", true);
        setResetAllAnimationAttackBools();
    }

    protected override void upSmash()
    {
        animator.SetBool("upSmash", true);
        setResetAllAnimationAttackBools();
    }



    protected override void natB()
    {
        animator.SetBool("natB", true);
        setResetAllAnimationAttackBools();
    }

    protected override void sideB()
    {
        animator.SetBool("sideB", true);
        setResetAllAnimationAttackBools();
    }

    protected override void downB()
    {
        animator.SetBool("downB", true);
        setResetAllAnimationAttackBools();
    }

    protected override void upB()
    {
        animator.SetBool("upB", true);
        setResetAllAnimationAttackBools();
    }



    protected override void aerialNatA()
    {
        animator.SetBool("aerialNatA", true);
        setResetAllAnimationAttackBools();
    }

    protected override void aerialForwardA()
    {
        animator.SetBool("aerialForwardA", true);
        setResetAllAnimationAttackBools();
    }

    protected override void aerialBackA()
    {
        animator.SetBool("aerialBackA", true);
        setResetAllAnimationAttackBools();
    }

    protected override void aerialDownA()
    {
        animator.SetBool("aerialDownA", true);
        setResetAllAnimationAttackBools();
    }
    */


}
